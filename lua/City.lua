--- @class City
local City = {}

--- @return bool
function City:IsNone() end

--- Deletes the city
---
--- Note that this does not fire the event ``SerialEventCityDestroyed``. The below example code can be used if you want
--- to manually fire the event.
---
--- .. code-block:: lua
---
---     include("FLuaVector.lua")
---
---     -- snip --
---
---     local plot = city:Plot()
---     local hex_pos = ToHexFromGrid(Vector2(plot:GetX(), plot:GetY()));
---     local player_id = city:GetOwner()
---     local city_id = city:GetID()
---     city:Kill()
---     Events.SerialEventCityDestroyed(hex_pos, player_id, city_id, -1)
---
--- Culture and tech penalties will not be reduced, as they are are based on the number of cities a player has *ever*
--- owned.
---
--- @return void
function City:Kill() end

--- @param great_person_unit UnitTypes
---
--- @return void
function City:CreateGreatGeneral(great_person_unit) end

--- @param great_person_unit UnitTypes
---
--- @return void
function City:CreateGreatAdmiral(great_person_unit) end

--- @param task TaskTypes
--- @param data1 int
--- @param data2 int
--- @param option bool
---
--- @return void
function City:DoTask(task, data1, data2, option) end

--- @return void
function City:ChooseProduction() end

--- Returns the plot index of a given plot
---
--- See :lua:meth:`City.GetCityIndexPlot`
---
--- @param plot Plot The plot
---
--- @return int
function City:GetCityPlotIndex(plot) end

--- Returns a plot given an plot index
---
--- See :lua:meth:`City.GetCityPlotIndex`
---
--- @param index int The plot index
---
--- @return Plot
function City:GetCityIndexPlot(index) end

--- Returns whether a city plot can be worked by a citizen
---
--- @param plot Plot The plot to check
---
--- @return bool
function City:CanWork(plot) end

--- Returns whether a city plot is blockaded and therefore cannot be worked
---
--- @param plot Plot The plot to check
---
--- @return bool
function City:IsPlotBlockaded(plot) end

--- See :lua:meth:`City.GetCityPlotIndex`
---
--- @param plot_index int The plot index
---
--- @return void
function City:ClearWorkingOverride(plot_index) end

--- @return int
function City:CountNumImprovedPlots() end

--- @return int
function City:CountNumWaterPlots() end

--- @return int
function City:CountNumRiverPlots() end

--- @return int
function City:FindPopulationRank() end

--- @param yield_type YieldTypes The yield type to query
---
--- @return int
function City:FindBaseYieldRateRank(yield_type) end

--- @param yield_type YieldTypes The yield type to query
---
--- @return int
function City:FindYieldRateRank(yield_type) end

--- Returns the unit type of the upgrade unit for a given unit, if any is available
---
--- If none are available, returns -1 (``NO_UNIT``)
---
--- @param unit_type UnitTypes The unit type to find the upgrade for
--- @param upgrade_count int|nil How many upgrades forward should we look for (defaults to ``1`` if not given)
---
--- @return UnitTypes
function City:AllUpgradesAvailable(unit_type, upgrade_count) end

--- Returns whether we have met or exceeded the max world wonder count in the city
---
--- This count is set by the ``MAX_WORLD_WONDERS_PER_CITY`` value in the :ref:`Defines <civ5_table_defines>` table of
--- the game database. If this value is ``-1``, the max world wonder count is unlimited.
---
--- This always returns ``false`` if you are playing a "One City Challenge" game.
---
--- @return bool
function City:IsWorldWondersMaxed() end

--- Returns whether we have met or exceeded the max team wonder count in the city
---
--- This count is set by the ``MAX_TEAM_WONDERS_PER_CITY`` value in the :ref:`Defines <civ5_table_defines>` table of the
--- game database. If this value is ``-1``, the max team wonder count is unlimited.
---
--- This always returns ``false`` if you are playing a "One City Challenge" game.
---
--- @return bool
function City:IsTeamWondersMaxed() end

--- Returns whether we have met or exceeded the max national wonder count in the city
---
--- This count is set by the ``MAX_NATIONAL_WONDERS_PER_CITY`` (or ``MAX_NATIONAL_WONDERS_PER_CITY_FOR_OCC`` for "One
--- City Challenge" games) value in the :ref:`Defines <civ5_table_defines>` table of the game database. If this value is
--- ``-1``, the max national wonder count is unlimited.
---
--- @return bool
function City:IsNationalWondersMaxed() end

--- Returns whether we have met or exceeded the max building count in the city
---
--- This count is set by the ``MAX_BUILDINGS_PER_CITY`` value in the :ref:`Defines <civ5_table_defines>` table of the
--- game database. If this value is ``-1``, the max team wonder count is unlimited.
---
--- This always returns ``false`` if you are playing a "One City Challenge" game.
---
--- @return bool
function City:IsBuildingsMaxed() end

--- @param unit_type UnitTypes The unit type to get the tooltip for
---
--- @return string
function City:CanTrainTooltip(unit_type) end

--- Returns whether a unit can be trained in the city
---
--- As part of this, the ``GameEvents.CityCanTrain`` will be fired. If any handler for that event returns ``false``,
--- this will return ``false``.
---
--- @param unit_type UnitTypes The unit type to check. Can be ``-1`` (``NO_UNIT``), in which case ``false`` will be returned.
--- @param continue int|nil Works like a boolean (provide zero or non-zero) (defaults to ``0`` if not given)
--- @param test_visible int|nil Works like a boolean (provide zero or non-zero) (defaults to ``0`` if not given)
--- @param ignore_cost int|nil Works like a boolean (provide zero or non-zero) (defaults to ``0`` if not given)
--- @param will_purchase int|nil Works like a boolean (provide zero or non-zero) (defaults to ``0`` if not given)
---
--- @return bool
function City:CanTrain(unit_type, continue, test_visible, ignore_cost, will_purchase) end

--- @param building_type BuildingTypes The building to query
---
--- @return string
function City:CanConstructTooltip(building_type) end

--- @param building_type BuildingTypes The building type to check. Can be ``-1`` (``NO_BUILDING``), in which case ``false`` will be returned.
--- @param continue int|nil Works like a boolean (provide zero or non-zero) (defaults to ``0`` if not given)
--- @param test_visible int|nil Works like a boolean (provide zero or non-zero) (defaults to ``0`` if not given)
--- @param ignore_cost int|nil Works like a boolean (provide zero or non-zero) (defaults to ``0`` if not given)
---
--- @return bool
function City:CanConstruct(building_type, continue, test_visible, ignore_cost) end

--- @param project_type ProjectTypes The project type to check.
--- @param continue int|nil Works like a boolean (provide zero or non-zero) (defaults to ``0`` if not given)
--- @param test_visible int|nil Works like a boolean (provide zero or non-zero) (defaults to ``0`` if not given)
--- @param ignore_cost int|nil Works like a boolean (provide zero or non-zero) (defaults to ``0`` if not given)
---
--- @return bool
function City:CanCreate(project_type, continue, test_visible) end

--- @param specialist SpecialistTypes
--- @param continue int|nil Works like a boolean (provide zero or non-zero) (defaults to ``0`` if not given)
---
--- @return bool
function City:CanPrepare(specialist, continue) end

--- @param process ProcessTypes
--- @param continue int|nil Works like a boolean (provide zero or non-zero) (defaults to ``0`` if not given)
---
--- @return bool
function City:CanMaintain(process, continue) end

--- @param unit_type UnitTypes
---
--- @return string
function City:GetPurchaseUnitTooltip(unit_type) end

--- @param unit_type UnitTypes
---
--- @return string
function City:GetFaithPurchaseUnitTooltip(unit_type) end

--- @param building_type BuildingTypes
---
--- @return string
function City:GetPurchaseBuildingTooltip(building_type) end

--- @param building_type BuildingTypes
---
--- @return string
function City:GetFaithPurchaseBuildingTooltip(building_type) end

--- @return bool
function City:CanJoin() end

--- @param building_type BuildingTypes
--- @param test_visible bool
---
--- @return bool
function City:IsBuildingLocalResourceValid(building_type, test_visible) end

--- Returns the resource that the city wants in order to enter "We love the king day"
---
--- TODO: Check if this param is optional or not? The C++ is unclear here...
---
--- @param hide_unknown bool|nil
---
--- @return ResourceTypes
function City:GetResourceDemanded(hide_unknown) end

--- Sets the resource that the city wants in order to enter "We love the king day"
---
--- @param resource_type ResourceTypes
---
--- @return void
function City:SetResourceDemanded(resource_type) end

--- Picks a resource for the city to want (for "We love the king day")
---
--- TODO: Check if this takes any param? The C++ is unclear here...
---
--- @param current_resource_invalid bool
---
--- @return void
function City:DoPickResourceDemanded(current_resource_invalid) end

--- Returns the number of turns until a new citizen appears in the city
---
--- @return int
function City:GetFoodTurnsLeft() end

--- Returns whether anything is in the production queue
---
--- @return bool
function City:IsProduction() end

--- Returns whether the current thing being produced is limited
---
--- Certain units, buildings, and projects have a limit of how many can exist in the world, how many can exist in a
--- team, and/or how many can exist per-player.
---
--- Global and team limits can be imposed on units and buildings by setting ``MaxGlobalInstances`` and
--- ``MaxTeamInstances`` to a value other than ``-1`` in the game database tables ``UnitClasses``, ``BuildingClasses``,
--- and ``Projects``. In addition, units and buildings can be limited per-player with the ``MaxPlayerInstances`` field.
---
--- @return bool
function City:IsProductionLimited() end

--- Returns whether the city is currently producing a unit
---
--- @return bool
function City:IsProductionUnit() end

--- Returns whether the city is currently producing a building
---
--- @return bool
function City:IsProductionBuilding() end

--- Returns whether the city is currently producing a project
---
--- @return bool
function City:IsProductionProject() end

--- Returns whether the city is currently producing a specialist
---
--- @return bool
function City:IsProductionSpecialist() end

--- Returns whether the city is currently spending production on a process (usually wealth or research)
---
--- @return bool
function City:IsProductionProcess() end

--- Returns whether a given order is allowed to be produced
---
--- Essentially a wrapper for :lua:meth:`City.CanTrain`, :lua:meth:`CanConstruct`, :lua:meth:`CanCreate`,
--- :lua:meth:`CanPrepare`, and :lua:meth:`CanMaintain` (Eg: calling
--- ``city:CanContinueProduction(OrderTypes.ORDER_TRAIN, GameInfo.Units.UNIT_WORKER.ID, 0, false)`` is equivalent to
--- calling ``city:CanTrain(GameInfo.Units.UNIT_WORKER.ID, 1)``).
---
--- @param order_type OrderTypes
--- @param data1 int Either a :lua:alias:`UnitTypes`, :lua:alias:`BuildingTypes`, :lua:alias:`ProjectTypes`, :lua:alias:`SpecialistTypes`, or :lua:alias:`ProcessTypes`, depending on the ``order_type`` provided
--- @param data2 int Unused
--- @param save bool Unused
---
--- @return bool
function City:CanContinueProduction(order_type, data1, data2, save) end

--- @param unit_type UnitTypes
---
--- @return int
function City:GetProductionExperience(unit_type) end

--- @param unit Unit
--- @param conscript bool|nil (defaults to ``false`` if not given)
---
--- @return void
function City:AddProductionExperience(unit, conscript) end

--- Returns the unit type the city is currently producing
---
--- Returns ``-1`` (``NO_UNIT``) if a unit is not currently being produced
---
--- @return UnitTypes
function City:GetProductionUnit() end

---
---
--- Returns ``-1`` (``NO_UNITAI``) if a unit is not currently being produced
---
--- @return UnitAITypes
function City:GetProductionUnitAI() end

--- Returns the building type the city is currently producing
---
--- Returns ``-1`` (``NO_BUILDING``) if a building is not currently being produced
---
--- @return BuildingTypes
function City:GetProductionBuilding() end

--- Returns the project type the city is currently producing
---
--- Returns ``-1`` (``NO_PROJECT``) if a project is not currently being produced
---
--- @return ProjectTypes
function City:GetProductionProject() end

--- Returns the specialist type the city is currently producing
---
--- Returns ``-1`` (``NO_SPECIALIST``) if a specialist is not currently being produced
---
--- @return SpecialistTypes
function City:GetProductionSpecialist() end

--- Returns the process type the city spending production on
---
--- Spending production on wealth or research are examples of processes.
---
--- Returns ``-1`` (``NO_PROCESS``) if production is not being spent on a process.
---
--- @return ProcessTypes
function City:GetProductionProcess() end

-- Commented out in C++
-- function City:GetProductionName() end

--- Returns the text key of the current item being produced
---
--- Returns an empty string if nothing is being produced
---
--- @return string
function City:GetProductionNameKey() end

--- Returns the turns left for the current item being produced
---
--- Returns ``0`` if the city is maintaining a process (such as wealth or research generation)
---
--- @return int
function City:GetGeneralProductionTurnsLeft() end

--- Returns the first index of a unit in the production queue
---
--- Zero indexed
---
--- Returns ``-1`` if the given ``unit_type`` is not in the queue
---
--- @param unit_type UnitTypes
---
--- @return int
function City:GetFirstUnitOrder(unit_type) end

--- Returns the first index of a project in the production queue
---
--- Zero indexed
---
--- Returns ``-1`` if the given ``project_type`` is not in the queue
---
--- @param project_type ProjectTypes
---
--- @return int
function City:GetFirstProjectOrder(project_type) end

--- Returns the first index of a specialist in the production queue
---
--- Zero indexed
---
--- Returns ``-1`` if the given ``specialist_type`` is not in the queue
---
--- @param specialist_type SpecialistTypes
---
--- @return int
function City:GetFirstSpecialistOrder(specialist_type) end

--- Returns the first index of a building in the production queue
---
--- Zero indexed
---
--- Returns ``-1`` if the given ``building_type`` is not in the queue
---
--- @param building_type BuildingTypes
---
--- @return int
function City:GetFirstBuildingOrder() end

--- Returns an order from the production queue
---
--- Returns 5 values - use it as ``local order_type, data1, data2, save, rush = city:GetOrderFromQueue(index)``
---
--- @param index int The index of the item to fetch from the queue (zero indexed)
--
--- @return OrderTypes The type of the order (``-1`` if the city doesn't exist or the index is out-of-bounds)
--- @return int data1 This is usually the ID of the item being produced, as a :lua:alias:`UnitTypes`, :lua:alias:`BuildingTypes`, :lua:alias:`ProjectTypes`, :lua:alias:`SpecialistTypes`, or :lua:alias:`ProcessTypes`, depending on the order type returned (``0`` if the city doesn't exist or the index is out-of-bounds)
--- @return int data2 (``0`` if the city doesn't exist or the index is out-of-bounds)
--- @return bool save (``false`` if the city doesn't exist or the index is out-of-bounds)
--- @return bool rush (``false`` if the city doesn't exist or the index is out-of-bounds)
function City:GetOrderFromQueue(index) end

--- @param unit_ai_type UnitAITypes
---
--- @return int
function City:GetNumTrainUnitAI(unit_ai_type) end

--- @return bool
function City:IsFoodProduction() end

--- @param unit_type UnitTypes
---
--- @return bool
function City:IsUnitFoodProduction() end

--- @return int
function City:GetProduction() end

--- @return int
function City:GetProductionTimes100() end

--- @return int
function City:GetProductionNeeded() end

--- @param unit_type UnitTypes
---
--- @return int
function City:GetUnitProductionNeeded(unit_types) end

--- @param building_type BuildingTypes
---
--- @return int
function City:GetBuildingProductionNeeded(building_type) end

--- @param project_type ProjectTypes
---
--- @return int
function City:GetProjectProductionNeeded(project_type) end

--- Returns the number of turns left to complete the current item being produced
---
--- Returns ``INT_MAX`` if not producing anything, or if maintaining a process (like wealth or research creation)
---
--- @return int
function City:GetProductionTurnsLeft() end

--- @param unit_type UnitTypes
--- @param num int|nil
---
--- @return int
function City:GetUnitProductionTurnsLeft(unit_type, num) end

--- @param building_type BuildingTypes
--- @param num int|nil
---
--- @return int
function City:GetBuildingProductionTurnsLeft() end

--- @param project_type ProjectTypes
--- @param num int|nil
---
--- @return int
function City:GetProjectProductionTurnsLeft() end

--- @param specialist_type SpecialistTypes
--- @param num int|nil
---
--- @return int
function City:GetSpecialistProductionTurnsLeft() end

--- Creates the space race trigger project in the city
---
--- ``SPACE_RACE_TRIGGER_PROJECT`` should be defined in either the :ref:`civ5_table_defines` or
--- :ref:`civ5_table_post_defines` table (usually the latter). This is usually the Apollo Program.
---
--- @return void
function City:CreateApolloProgram() end

--- Returns whether the city allowed to purchase a given item
---
--- Only one of ``unit_type``, ``building_type``, ``project_type``, or ``yield_type`` should be specified - the other
--- values should be set to ``-1``.
---
--- @param test_purchase_cost bool Should the game check if the player has enough gold/faith to purchase the given item? If the player doesn't have enough, ``false`` will be returned.
--- @param test_trainable bool
--- @param unit_type UnitTypes The unit type to check, or ``-1`` if not checking a unit
--- @param building_type BuildingTypes The building type to check, or ``-1`` if not checking a building
--- @param project_type ProjectTypes The project type to check, or ``-1`` if not checking a project
--- @param yield_type YieldTypes The yield type to check, or ``-1`` if not checking a yield
---
--- @return bool
function City:IsCanPurchase(test_purchase_cost, test_trainable, unit_type, building_type, project_type, yield_type) end

--- Returns the cost to purchase a unit with gold
---
--- Rounded down to a multiple of the ``GOLD_PURCHASE_VISIBLE_DIVISOR`` value (found in the :ref:`Defines
--- <civ5_table_defines>` table)
---
--- @param unit_type UnitTypes The unit type to check
---
--- @return int
function City:GetUnitPurchaseCost(unit_type) end

--- Returns the cost to purchase a unit with faith
---
--- Rounded down to a multiple of the ``GOLD_PURCHASE_VISIBLE_DIVISOR`` value (found in the :ref:`Defines
--- <civ5_table_defines>` table) (That GOLD is not a typo)
---
--- @param unit_type UnitTypes The unit type to check
--- @param include_belief_discounts bool Whether belief discounts should be factored in
---
--- @return int
function City:GetUnitFaithPurchaseCost(unit_type, include_belief_discounts) end

function City:GetBuildingPurchaseCost() end
function City:GetBuildingFaithPurchaseCost() end
function City:GetProjectPurchaseCost() end

function City:SetProduction() end
function City:ChangeProduction() end

function City:GetYieldModifierTooltip() end
function City:GetProductionModifier() end

function City:GetCurrentProductionDifference() end
function City:GetRawProductionDifference() end
function City:GetCurrentProductionDifferenceTimes100() end
function City:GetRawProductionDifferenceTimes100() end
function City:GetUnitProductionModifier() end
function City:GetBuildingProductionModifier() end
function City:GetProjectProductionModifier() end
function City:GetSpecialistProductionModifier() end

function City:GetExtraProductionDifference() end

function City:CanHurry() end
function City:Hurry() end
function City:GetConscriptUnit() end
function City:GetConscriptPopulation() end
function City:ConscriptMinCityPopulation() end
function City:CanConscript() end
function City:Conscript() end
function City:GetResourceYieldRateModifier() end
function City:GetHandicapType() end
function City:GetCivilizationType() end
function City:GetPersonalityType() end
function City:GetArtStyleType() end
function City:GetCitySizeType() end

function City:IsBarbarian() end
function City:IsHuman() end
function City:IsVisible() end

--- Returns a boolean depending on if the selected city is marked as the current capital.

--- @return bool
function City:IsCapital() end

--- Returns a boolean depending on if the selected city is marked as the first founded city of a civilization (referred to as the first capital).

--- @return bool
function City:IsOriginalCapital() end

--- Returns a boolean depending on if the selected city is marked as the first founded city of a major player ( You the player, or an AI playing as Atilla...).

--- @return bool
function City:IsOriginalMajorCapital() end

--- Does this city reside next to a body of water?

--- @return bool
function City:IsCoastal() end

function City:FoodConsumption() end
function City:FoodDifference() end
function City:FoodDifferenceTimes100() end
function City:GrowthThreshold() end
function City:ProductionLeft() end
function City:HurryCost() end
function City:HurryGold() end
function City:HurryPopulation() end
function City:HurryProduction() end
function City:MaxHurryPopulation() end

function City:GetNumBuilding() end
function City:IsHasBuilding() end
function City:GetNumActiveBuilding() end
function City:GetID() end
function City:GetX() end
function City:GetY() end
function City:At() end
function City:AtPlot() end
function City:Plot() end
function City:Area() end
function City:WaterArea() end
function City:GetRallyPlot() end

function City:CanBuyPlot() end
function City:CanBuyPlotAt() end
function City:GetNextBuyablePlot() end
function City:GetBuyablePlotList() end
function City:GetBuyPlotCost() end
function City:CanBuyAnyPlot() end

function City:GetGarrisonedUnit() end

function City:GetGameTurnFounded() end
function City:GetGameTurnAcquired() end
function City:GetGameTurnLastExpanded() end

--- Returns the (small number) population of the city
---
--- Returns the number of workable citizens in a city (Eg: ``5``)
---
--- @return int
function City:GetPopulation() end

function City:SetPopulation() end
function City:ChangePopulation() end

--- Returns the (large number) population of the city
---
--- Eg: ``5`` workable citizens gives a "real" population of ``90597``
---
--- This is calculated as ``floor((population ^ 2.8) * 1000)``
---
--- @return int
function City:GetRealPopulation() end

function City:GetHighestPopulation() end
function City:SetHighestPopulation() end
-- Commented out in C++
-- function City:GetWorkingPopulation() end
-- Commented out in C++
-- function City:GetSpecialistPopulation() end
function City:GetNumGreatPeople() end
function City:GetBaseGreatPeopleRate() end
function City:GetGreatPeopleRate() end
function City:GetTotalGreatPeopleRateModifier() end
function City:ChangeBaseGreatPeopleRate() end
function City:GetGreatPeopleRateModifier() end

function City:GetJONSCultureStored() end
function City:SetJONSCultureStored() end
function City:ChangeJONSCultureStored() end
function City:GetJONSCultureLevel() end
function City:SetJONSCultureLevel() end
function City:ChangeJONSCultureLevel() end
function City:DoJONSCultureLevelIncrease() end
function City:GetJONSCultureThreshold() end

function City:GetJONSCulturePerTurn() end

function City:GetBaseJONSCulturePerTurn() end
function City:GetJONSCulturePerTurnFromBuildings() end
function City:ChangeJONSCulturePerTurnFromBuildings() end
function City:GetJONSCulturePerTurnFromPolicies() end
function City:ChangeJONSCulturePerTurnFromPolicies() end
function City:GetJONSCulturePerTurnFromSpecialists() end
function City:ChangeJONSCulturePerTurnFromSpecialists() end
function City:GetJONSCulturePerTurnFromGreatWorks() end
function City:GetJONSCulturePerTurnFromTraits() end
function City:GetJONSCulturePerTurnFromReligion() end
function City:GetJONSCulturePerTurnFromLeagues() end

function City:GetCultureRateModifier() end
function City:ChangeCultureRateModifier() end

function City:GetNumGreatWorks() end
function City:GetNumGreatWorkSlots() end
function City:GetBaseTourism() end
function City:GetTourismMultiplier() end
function City:GetTourismTooltip() end
function City:GetFilledSlotsTooltip() end
function City:GetTotalSlotsTooltip() end
function City:ClearGreatWorks() end
function City:GetFaithBuildingTourism() end

function City:IsThemingBonusPossible() end
function City:GetThemingBonus() end
function City:GetThemingTooltip() end

function City:GetFaithPerTurn() end
function City:GetFaithPerTurnFromBuildings() end
function City:GetFaithPerTurnFromPolicies() end
function City:GetFaithPerTurnFromTraits() end
function City:GetFaithPerTurnFromReligion() end

function City:IsReligionInCity() end
function City:IsHolyCityForReligion() end
function City:IsHolyCityAnyReligion() end

function City:GetNumFollowers() end

--- Returns a ReligionType (Integer) for the current dominant religion in the city.

--- @return ReligionType

function City:GetReligiousMajority() end
function City:GetSecondaryReligion() end
function City:GetSecondaryReligionPantheonBelief() end
function City:GetPressurePerTurn() end
function City:ConvertPercentFollowers() end
function City:AdoptReligionFully() end
function City:GetReligionBuildingClassHappiness() end
function City:GetReligionBuildingClassYieldChange() end
function City:GetLeagueBuildingClassYieldChange() end
function City:GetNumTradeRoutesAddingPressure() end

function City:GetNumWorldWonders() end
function City:GetNumTeamWonders() end
function City:GetNumNationalWonders() end
function City:GetNumBuildings() end

function City:GetWonderProductionModifier() end
function City:ChangeWonderProductionModifier() end

function City:GetLocalResourceWonderProductionMod() end

function City:ChangeHealRate() end

function City:IsNoOccupiedUnhappiness() end

function City:GetFood() end
function City:GetFoodTimes100() end
function City:SetFood() end
function City:ChangeFood() end
function City:GetFoodKept() end
function City:GetMaxFoodKeptPercent() end
function City:GetOverflowProduction() end
function City:SetOverflowProduction() end
function City:GetFeatureProduction() end
function City:SetFeatureProduction() end
function City:GetMilitaryProductionModifier() end
function City:GetSpaceProductionModifier() end
function City:GetBuildingDefense() end
function City:GetFreeExperience() end
function City:GetNukeModifier() end

-- Commented out in C++
-- function City:GetFreeSpecialist() end

function City:IsResistance() end
function City:GetResistanceTurns() end
function City:ChangeResistanceTurns() end

function City:IsRazing() end
function City:GetRazingTurns() end
function City:ChangeRazingTurns() end

function City:IsOccupied() end
function City:SetOccupied() end

function City:IsPuppet() end
function City:SetPuppet() end

function City:GetHappinessFromBuildings() end
function City:GetHappiness() end
function City:GetLocalHappiness() end

function City:IsNeverLost() end
function City:SetNeverLost() end
function City:IsDrafted() end
function City:SetDrafted() end

function City:IsBlockaded() end

function City:GetWeLoveTheKingDayCounter() end
function City:SetWeLoveTheKingDayCounter() end
function City:ChangeWeLoveTheKingDayCounter() end

function City:GetNumThingsProduced() end

function City:IsProductionAutomated() end
function City:SetProductionAutomated() end
function City:SetCitySizeBoost() end

--- Returns the player ID that owns the city
---
--- @return PlayerTypes
function City:GetOwner() end

--- Returns the team ID of the owning player
---
--- Equivilent to ``Players[city:GetOwner()]:GetTeam()``
---
--- @return TeamTypes
function City:GetTeam() end

function City:GetPreviousOwner() end
function City:GetOriginalOwner() end
function City:GetSeaPlotYield() end
function City:GetRiverPlotYield() end
function City:GetLakePlotYield() end

function City:GetBaseYieldRate() end

function City:GetBaseYieldRateFromTerrain() end
function City:ChangeBaseYieldRateFromTerrain() end

function City:GetBaseYieldRateFromBuildings() end
function City:ChangeBaseYieldRateFromBuildings() end

function City:GetBaseYieldRateFromSpecialists() end
function City:ChangeBaseYieldRateFromSpecialists() end

function City:GetBaseYieldRateFromMisc() end
function City:ChangeBaseYieldRateFromMisc() end

function City:GetBaseYieldRateFromReligion() end
function City:ChangeBaseYieldRateFromReligion() end

function City:GetYieldPerPopTimes100() end

function City:GetBaseYieldRateModifier() end
function City:GetYieldRate() end
function City:GetYieldRateTimes100() end
function City:GetYieldRateModifier() end

function City:GetExtraSpecialistYield() end
function City:GetExtraSpecialistYieldOfType() end

function City:GetDomainFreeExperience() end
function City:GetDomainProductionModifier() end

function City:IsEverOwned() end

function City:IsRevealed() end
function City:SetRevealed() end
function City:GetNameKey() end
function City:GetName() end
function City:SetName() end
function City:IsHasResourceLocal() end
function City:GetBuildingProduction() end
function City:SetBuildingProduction() end
function City:ChangeBuildingProduction() end
function City:GetBuildingProductionTime() end
function City:SetBuildingProductionTime() end
function City:ChangeBuildingProductionTime() end
function City:GetBuildingOriginalOwner() end
function City:GetBuildingOriginalTime() end
function City:GetUnitProduction() end
function City:SetUnitProduction() end
function City:ChangeUnitProduction() end

function City:IsCanAddSpecialistToBuilding() end
function City:GetSpecialistUpgradeThreshold() end
function City:GetNumSpecialistsAllowedByBuilding() end
function City:GetSpecialistCount() end
function City:GetSpecialistGreatPersonProgress() end
function City:GetSpecialistGreatPersonProgressTimes100() end
function City:ChangeSpecialistGreatPersonProgressTimes100() end
function City:GetNumSpecialistsInBuilding() end
function City:DoReallocateCitizens() end
function City:DoVerifyWorkingPlots() end
function City:IsNoAutoAssignSpecialists() end

function City:GetFocusType() end
function City:SetFocusType() end

function City:IsForcedAvoidGrowth() end

function City:GetUnitCombatFreeExperience() end
function City:GetFreePromotionCount() end
function City:IsFreePromotion() end
function City:GetSpecialistFreeExperience() end

function City:UpdateStrengthValue() end
function City:GetStrengthValue() end

function City:GetDamage() end
function City:SetDamage() end
function City:ChangeDamage() end
function City:GetMaxHitPoints() end
function City:CanRangeStrike() end
function City:CanRangeStrikeNow() end
function City:CanRangeStrikeAt() end
function City:HasPerformedRangedStrikeThisTurn() end
function City:RangeCombatUnitDefense() end
function City:RangeCombatDamage() end
function City:GetAirStrikeDefenseDamage() end

function City:IsWorkingPlot() end
function City:AlterWorkingPlot() end
function City:IsForcedWorkingPlot() end

--- Gets the number of buildings that exist for the specified building ID. Usually this value will be 0 or 1, unless "NoLimit" in "BuildingClasses" is True.

--- @param BuildingType In the specified city, what building ID to analyse?
--- @return int 

function City:GetNumRealBuilding(BuildingType) end

--- Sets the number of one type of building that should exist in the specified city. Returns no value.

--- @param BuildingType In the specified city, what building ID to change the state of?
--- @param int In the specified city, 0 means that the building will not exist. 1 Means that the building will exist. If in "BuildingClasses", the parameter "NoLimit" is set to 1 (True), anything more than 1 for this value will duplicate the building effects according to the set value.

function City:SetNumRealBuilding(BuildingType, int) end
function City:GetNumFreeBuilding() end
function City:IsBuildingSellable() end
function City:GetSellBuildingRefund() end
function City:GetTotalBaseBuildingMaintenance() end
function City:GetBuildingGreatWork() end
function City:SetBuildingGreatWork() end
function City:IsHoldingGreatWork() end
function City:GetNumGreatWorksInBuilding() end

function City:ClearOrderQueue() end
function City:PushOrder() end
function City:PopOrder() end
function City:GetOrderQueueLength() end

function City:GetBuildingYieldChange() end
function City:SetBuildingYieldChange() end

function City:GetBuildingEspionageModifier() end
function City:GetBuildingGlobalEspionageModifier() end

function City:GetNumCityPlots() end
function City:CanPlaceUnitHere() end

function City:GetSpecialistYield() end
function City:GetCultureFromSpecialist() end

function City:GetNumForcedWorkingPlots() end

function City:GetReligionCityRangeStrikeModifier() end

return City
