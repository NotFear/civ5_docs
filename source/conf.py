# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'Civilization'
copyright = '2021, some people'
author = 'some more people'
version = '5'
release = version


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinxcontrib.luadomain',
    'sphinx_lua',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

rst_prolog = """
.. |icon_string| image:: /_static/image/data_types/string.png
.. |icon_int| image:: /_static/image/data_types/int.png
.. |icon_bool| image:: /_static/image/data_types/bool.png
.. |icon_variant| image:: /_static/image/data_types/variant.png

.. |civ5_gak| replace:: :ref:`G&K <civ5_gak>`
.. |civ5_bnw| replace:: :ref:`BNW <civ5_bnw>`

.. |civ5_icon_alpha| image:: /_static/image/civ5/font_icon/ICON_ALPHA.png
.. |civ5_icon_blockaded| image:: /_static/image/civ5/font_icon/ICON_BLOCKADED.png
.. |civ5_icon_bullet| image:: /_static/image/civ5/font_icon/ICON_BULLET.png
.. |civ5_icon_capital| image:: /_static/image/civ5/font_icon/ICON_CAPITAL.png
.. |civ5_icon_citizen| image:: /_static/image/civ5/font_icon/ICON_CITIZEN.png
.. |civ5_icon_city_state| image:: /_static/image/civ5/font_icon/ICON_CITY_STATE.png
.. |civ5_icon_connected| image:: /_static/image/civ5/font_icon/ICON_CONNECTED.png
.. |civ5_icon_culture| image:: /_static/image/civ5/font_icon/ICON_CULTURE.png
.. |civ5_icon_flower| image:: /_static/image/civ5/font_icon/ICON_FLOWER.png
.. |civ5_icon_food| image:: /_static/image/civ5/font_icon/ICON_FOOD.png
.. |civ5_icon_golden_age| image:: /_static/image/civ5/font_icon/ICON_GOLDEN_AGE.png
.. |civ5_icon_gold| image:: /_static/image/civ5/font_icon/ICON_GOLD.png
.. |civ5_icon_great_people| image:: /_static/image/civ5/font_icon/ICON_GREAT_PEOPLE.png
.. |civ5_icon_happiness_1| image:: /_static/image/civ5/font_icon/ICON_HAPPINESS_1.png
.. |civ5_icon_happiness_2| image:: /_static/image/civ5/font_icon/ICON_HAPPINESS_2.png
.. |civ5_icon_happiness_3| image:: /_static/image/civ5/font_icon/ICON_HAPPINESS_3.png
.. |civ5_icon_happiness_4| image:: /_static/image/civ5/font_icon/ICON_HAPPINESS_4.png
.. |civ5_icon_influence| image:: /_static/image/civ5/font_icon/ICON_INFLUENCE.png
.. |civ5_icon_locked| image:: /_static/image/civ5/font_icon/ICON_LOCKED.png
.. |civ5_icon_minus| image:: /_static/image/civ5/font_icon/ICON_MINUS.png
.. |civ5_icon_moves| image:: /_static/image/civ5/font_icon/ICON_MOVES.png
.. |civ5_icon_mushroom| image:: /_static/image/civ5/font_icon/ICON_MUSHROOM.png
.. |civ5_icon_occupied| image:: /_static/image/civ5/font_icon/ICON_OCCUPIED.png
.. |civ5_icon_omega| image:: /_static/image/civ5/font_icon/ICON_OMEGA.png
.. |civ5_icon_peace| image:: /_static/image/civ5/font_icon/ICON_PEACE.png
.. |civ5_icon_pirate| image:: /_static/image/civ5/font_icon/ICON_PIRATE.png
.. |civ5_icon_plus| image:: /_static/image/civ5/font_icon/ICON_PLUS.png
.. |civ5_icon_production| image:: /_static/image/civ5/font_icon/ICON_PRODUCTION.png
.. |civ5_icon_puppet| image:: /_static/image/civ5/font_icon/ICON_PUPPET.png
.. |civ5_icon_range_strength| image:: /_static/image/civ5/font_icon/ICON_RANGE_STRENGTH.png
.. |civ5_icon_razing| image:: /_static/image/civ5/font_icon/ICON_RAZING.png
.. |civ5_icon_res_aluminum| image:: /_static/image/civ5/font_icon/ICON_RES_ALUMINUM.png
.. |civ5_icon_res_banana| image:: /_static/image/civ5/font_icon/ICON_RES_BANANA.png
.. |civ5_icon_res_coal| image:: /_static/image/civ5/font_icon/ICON_RES_COAL.png
.. |civ5_icon_res_cotton| image:: /_static/image/civ5/font_icon/ICON_RES_COTTON.png
.. |civ5_icon_res_cow| image:: /_static/image/civ5/font_icon/ICON_RES_COW.png
.. |civ5_icon_res_deer| image:: /_static/image/civ5/font_icon/ICON_RES_DEER.png
.. |civ5_icon_res_dye| image:: /_static/image/civ5/font_icon/ICON_RES_DYE.png
.. |civ5_icon_research| image:: /_static/image/civ5/font_icon/ICON_RESEARCH.png
.. |civ5_icon_res_fish| image:: /_static/image/civ5/font_icon/ICON_RES_FISH.png
.. |civ5_icon_res_fur| image:: /_static/image/civ5/font_icon/ICON_RES_FUR.png
.. |civ5_icon_res_gems| image:: /_static/image/civ5/font_icon/ICON_RES_GEMS.png
.. |civ5_icon_res_gold| image:: /_static/image/civ5/font_icon/ICON_RES_GOLD.png
.. |civ5_icon_res_horse| image:: /_static/image/civ5/font_icon/ICON_RES_HORSE.png
.. |civ5_icon_res_incense| image:: /_static/image/civ5/font_icon/ICON_RES_INCENSE.png
.. |civ5_icon_res_iron| image:: /_static/image/civ5/font_icon/ICON_RES_IRON.png
.. |civ5_icon_resistance| image:: /_static/image/civ5/font_icon/ICON_RESISTANCE.png
.. |civ5_icon_res_ivory| image:: /_static/image/civ5/font_icon/ICON_RES_IVORY.png
.. |civ5_icon_res_marble| image:: /_static/image/civ5/font_icon/ICON_RES_MARBLE.png
.. |civ5_icon_res_oil| image:: /_static/image/civ5/font_icon/ICON_RES_OIL.png
.. |civ5_icon_res_pearls| image:: /_static/image/civ5/font_icon/ICON_RES_PEARLS.png
.. |civ5_icon_res_sheep| image:: /_static/image/civ5/font_icon/ICON_RES_SHEEP.png
.. |civ5_icon_res_silk| image:: /_static/image/civ5/font_icon/ICON_RES_SILK.png
.. |civ5_icon_res_silver| image:: /_static/image/civ5/font_icon/ICON_RES_SILVER.png
.. |civ5_icon_res_spices| image:: /_static/image/civ5/font_icon/ICON_RES_SPICES.png
.. |civ5_icon_res_sugar| image:: /_static/image/civ5/font_icon/ICON_RES_SUGAR.png
.. |civ5_icon_res_uranium| image:: /_static/image/civ5/font_icon/ICON_RES_URANIUM.png
.. |civ5_icon_res_whale| image:: /_static/image/civ5/font_icon/ICON_RES_WHALE.png
.. |civ5_icon_res_wheat| image:: /_static/image/civ5/font_icon/ICON_RES_WHEAT.png
.. |civ5_icon_res_wine| image:: /_static/image/civ5/font_icon/ICON_RES_WINE.png
.. |civ5_icon_star| image:: /_static/image/civ5/font_icon/ICON_STAR.png
.. |civ5_icon_strength| image:: /_static/image/civ5/font_icon/ICON_STRENGTH.png
.. |civ5_icon_team_10| image:: /_static/image/civ5/font_icon/ICON_TEAM_10.png
.. |civ5_icon_team_11| image:: /_static/image/civ5/font_icon/ICON_TEAM_11.png
.. |civ5_icon_team_1| image:: /_static/image/civ5/font_icon/ICON_TEAM_1.png
.. |civ5_icon_team_2| image:: /_static/image/civ5/font_icon/ICON_TEAM_2.png
.. |civ5_icon_team_3| image:: /_static/image/civ5/font_icon/ICON_TEAM_3.png
.. |civ5_icon_team_4| image:: /_static/image/civ5/font_icon/ICON_TEAM_4.png
.. |civ5_icon_team_5| image:: /_static/image/civ5/font_icon/ICON_TEAM_5.png
.. |civ5_icon_team_6| image:: /_static/image/civ5/font_icon/ICON_TEAM_6.png
.. |civ5_icon_team_7| image:: /_static/image/civ5/font_icon/ICON_TEAM_7.png
.. |civ5_icon_team_8| image:: /_static/image/civ5/font_icon/ICON_TEAM_8.png
.. |civ5_icon_team_9| image:: /_static/image/civ5/font_icon/ICON_TEAM_9.png
.. |civ5_icon_team_usa| image:: /_static/image/civ5/font_icon/ICON_TEAM_USA.png
.. |civ5_icon_trade| image:: /_static/image/civ5/font_icon/ICON_TRADE.png
.. |civ5_icon_trade_white| image:: /_static/image/civ5/font_icon/ICON_TRADE_WHITE.png
.. |civ5_icon_war| image:: /_static/image/civ5/font_icon/ICON_WAR.png
.. |civ5_icon_wtf1| image:: /_static/image/civ5/font_icon/ICON_WTF1.png
.. |civ5_icon_wtf2| image:: /_static/image/civ5/font_icon/ICON_WTF2.png

.. |civ5_icon_res_stone| image:: /_static/image/civ5/font_icon/ICON_RES_STONE.png
.. |civ5_icon_swap| image:: /_static/image/civ5/font_icon/ICON_SWAP.png

.. |civ5_icon_res_citrus| image:: /_static/image/civ5/font_icon/ICON_RES_CITRUS.png
.. |civ5_icon_res_copper| image:: /_static/image/civ5/font_icon/ICON_RES_COPPER.png
.. |civ5_icon_res_crab| image:: /_static/image/civ5/font_icon/ICON_RES_CRAB.png
.. |civ5_icon_res_salt| image:: /_static/image/civ5/font_icon/ICON_RES_SALT.png
.. |civ5_icon_res_truffles| image:: /_static/image/civ5/font_icon/ICON_RES_TRUFFLES.png
.. |civ5_icon_res_jewelry| image:: /_static/image/civ5/font_icon/ICON_RES_JEWELRY.png
.. |civ5_icon_res_porcelain| image:: /_static/image/civ5/font_icon/ICON_RES_PORCELAIN.png
.. |civ5_icon_religion_buddhism| image:: /_static/image/civ5/font_icon/ICON_RELIGION_BUDDHISM.png
.. |civ5_icon_religion_christianity| image:: /_static/image/civ5/font_icon/ICON_RELIGION_CHRISTIANITY.png
.. |civ5_icon_religion_confucianism| image:: /_static/image/civ5/font_icon/ICON_RELIGION_CONFUCIANISM.png
.. |civ5_icon_religion_hinduism| image:: /_static/image/civ5/font_icon/ICON_RELIGION_HINDUISM.png
.. |civ5_icon_religion_islam| image:: /_static/image/civ5/font_icon/ICON_RELIGION_ISLAM.png
.. |civ5_icon_religion_judaism| image:: /_static/image/civ5/font_icon/ICON_RELIGION_JUDAISM.png
.. |civ5_icon_religion_pantheon| image:: /_static/image/civ5/font_icon/ICON_RELIGION_PANTHEON.png
.. |civ5_icon_religion_shinto| image:: /_static/image/civ5/font_icon/ICON_RELIGION_SHINTO.png
.. |civ5_icon_religion_sikhism| image:: /_static/image/civ5/font_icon/ICON_RELIGION_SIKHISM.png
.. |civ5_icon_religion_taoism| image:: /_static/image/civ5/font_icon/ICON_RELIGION_TAOISM.png
.. |civ5_icon_religion_tengriism| image:: /_static/image/civ5/font_icon/ICON_RELIGION_TENGRIISM.png
.. |civ5_icon_religion_zoroastrianism| image:: /_static/image/civ5/font_icon/ICON_RELIGION_ZOROASTRIANISM.png
.. |civ5_icon_religion_orthodox| image:: /_static/image/civ5/font_icon/ICON_RELIGION_ORTHODOX.png
.. |civ5_icon_religion_protestant| image:: /_static/image/civ5/font_icon/ICON_RELIGION_PROTESTANT.png
.. |civ5_icon_religion| image:: /_static/image/civ5/font_icon/ICON_RELIGION.png
.. |civ5_icon_missionary| image:: /_static/image/civ5/font_icon/ICON_MISSIONARY.png
.. |civ5_icon_prophet| image:: /_static/image/civ5/font_icon/ICON_PROPHET.png
.. |civ5_icon_inquisitor| image:: /_static/image/civ5/font_icon/ICON_INQUISITOR.png
.. |civ5_icon_spy| image:: /_static/image/civ5/font_icon/ICON_SPY.png
.. |civ5_icon_view_city| image:: /_static/image/civ5/font_icon/ICON_VIEW_CITY.png
.. |civ5_icon_worker| image:: /_static/image/civ5/font_icon/ICON_WORKER.png
.. |civ5_icon_denounce| image:: /_static/image/civ5/font_icon/ICON_DENOUNCE.png
.. |civ5_icon_invest| image:: /_static/image/civ5/font_icon/ICON_INVEST.png

.. |civ5_icon_silver_fist| image:: /_static/image/civ5/font_icon/ICON_SILVER_FIST.png
.. |civ5_icon_checkbox| image:: /_static/image/civ5/font_icon/ICON_CHECKBOX.png
.. |civ5_icon_tourism| image:: /_static/image/civ5/font_icon/ICON_TOURISM.png
.. |civ5_icon_international_trade| image:: /_static/image/civ5/font_icon/ICON_INTERNATIONAL_TRADE.png
.. |civ5_icon_great_work| image:: /_static/image/civ5/font_icon/ICON_GREAT_WORK.png
.. |civ5_icon_arrow_left| image:: /_static/image/civ5/font_icon/ICON_ARROW_LEFT.png
.. |civ5_icon_arrow_right| image:: /_static/image/civ5/font_icon/ICON_MOVES.png
.. |civ5_icon_victory_space| image:: /_static/image/civ5/font_icon/ICON_VICTORY_SPACE.png
.. |civ5_icon_victory_culture| image:: /_static/image/civ5/font_icon/ICON_VICTORY_CULTURE.png
.. |civ5_icon_victory_diplomacy| image:: /_static/image/civ5/font_icon/ICON_VICTORY_DIPLOMACY.png
.. |civ5_icon_victory_domination| image:: /_static/image/civ5/font_icon/ICON_VICTORY_DOMINATION.png
.. |civ5_icon_turns_remaining| image:: /_static/image/civ5/font_icon/ICON_SWAP.png
.. |civ5_icon_great_engineer| image:: /_static/image/civ5/font_icon/ICON_GREAT_ENGINEER.png
.. |civ5_icon_great_general| image:: /_static/image/civ5/font_icon/ICON_GREAT_GENERAL.png
.. |civ5_icon_great_scientist| image:: /_static/image/civ5/font_icon/ICON_GREAT_SCIENTIST.png
.. |civ5_icon_great_merchant| image:: /_static/image/civ5/font_icon/ICON_GREAT_MERCHANT.png
.. |civ5_icon_great_artist| image:: /_static/image/civ5/font_icon/ICON_GREAT_ARTIST.png
.. |civ5_icon_great_musician| image:: /_static/image/civ5/font_icon/ICON_GREAT_MUSICIAN.png
.. |civ5_icon_great_writer| image:: /_static/image/civ5/font_icon/ICON_GREAT_WRITER.png
.. |civ5_icon_great_admiral| image:: /_static/image/civ5/font_icon/ICON_GREAT_ADMIRAL.png
.. |civ5_icon_great_merchant_venice| image:: /_static/image/civ5/font_icon/ICON_GREAT_MERCHANT_VENICE.png
.. |civ5_icon_great_explorer| image:: /_static/image/civ5/font_icon/ICON_GREAT_EXPLORER.png
.. |civ5_icon_res_cloves| image:: /_static/image/civ5/font_icon/ICON_RES_CLOVES.png
.. |civ5_icon_res_nutmeg| image:: /_static/image/civ5/font_icon/ICON_RES_NUTMEG.png
.. |civ5_icon_res_pepper| image:: /_static/image/civ5/font_icon/ICON_RES_PEPPER.png
.. |civ5_icon_diplomat| image:: /_static/image/civ5/font_icon/ICON_DIPLOMAT.png
.. |civ5_icon_res_artifacts| image:: /_static/image/civ5/font_icon/ICON_RES_ARTIFACTS.png
.. |civ5_icon_trophy_gold| image:: /_static/image/civ5/font_icon/ICON_TROPHY_GOLD.png
.. |civ5_icon_trophy_silver| image:: /_static/image/civ5/font_icon/ICON_TROPHY_SILVER.png
.. |civ5_icon_trophy_bronze| image:: /_static/image/civ5/font_icon/ICON_TROPHY_BRONZE.png
.. |civ5_icon_res_manpower| image:: /_static/image/civ5/font_icon/ICON_RES_MANPOWER.png
.. |civ5_icon_res_hidden_artifacts| image:: /_static/image/civ5/font_icon/ICON_RES_HIDDEN_ARTIFACTS.png
.. |civ5_icon_ideology_order| image:: /_static/image/civ5/font_icon/ICON_IDEOLOGY_ORDER.png
.. |civ5_icon_ideology_freedom| image:: /_static/image/civ5/font_icon/ICON_IDEOLOGY_FREEDOM.png
.. |civ5_icon_ideology_autocracy| image:: /_static/image/civ5/font_icon/ICON_IDEOLOGY_AUTOCRACY.png
.. |civ5_icon_res_cocoa| image:: /_static/image/civ5/font_icon/ICON_RES_COCOA.png
.. |civ5_icon_res_bison| image:: /_static/image/civ5/font_icon/ICON_RES_BISON.png

.. role:: civ5_color_clear
.. role:: civ5_color_alpha_grey
.. role:: civ5_color_white
.. role:: civ5_color_black
.. role:: civ5_color_dark_grey
.. role:: civ5_color_grey
.. role:: civ5_color_light_grey
.. role:: civ5_color_green
.. role:: civ5_color_blue
.. role:: civ5_color_xp_blue
.. role:: civ5_color_cyan
.. role:: civ5_color_yellow
.. role:: civ5_color_magenta
.. role:: civ5_color_yield_food
.. role:: civ5_color_yield_production
.. role:: civ5_color_yield_gold
.. role:: civ5_color_city_blue
.. role:: civ5_color_city_grey
.. role:: civ5_color_city_brown
.. role:: civ5_color_city_green
.. role:: civ5_color_font_red
.. role:: civ5_color_font_green
.. role:: civ5_color_research_stored
.. role:: civ5_color_research_rate
.. role:: civ5_color_culture_stored
.. role:: civ5_color_culture_rate
.. role:: civ5_color_great_people_stored
.. role:: civ5_color_great_people_rate
.. role:: civ5_color_negative_rate
.. role:: civ5_color_empty
.. role:: civ5_color_popup_text
.. role:: civ5_color_popup_selected
.. role:: civ5_color_tech_text
.. role:: civ5_color_unit_text
.. role:: civ5_color_building_text
.. role:: civ5_color_project_text
.. role:: civ5_color_highlight_text
.. role:: civ5_color_alt_highlight_text
.. role:: civ5_color_warning_text
.. role:: civ5_color_positive_text
.. role:: civ5_color_negative_text
.. role:: civ5_color_brown_text
.. role:: civ5_color_selected_text
.. role:: civ5_color_water_text
.. role:: civ5_color_menu_blue
.. role:: civ5_color_dawn_of_man_text
.. role:: civ5_color_advisor_highlight_text
.. role:: civ5_color_tech_green
.. role:: civ5_color_tech_blue
.. role:: civ5_color_tech_working
.. role:: civ5_color_tech_black
.. role:: civ5_color_tech_red
.. role:: civ5_color_red
.. role:: civ5_color_player_black
.. role:: civ5_color_player_black_text
.. role:: civ5_color_player_blue
.. role:: civ5_color_player_light_blue_text
.. role:: civ5_color_player_brown
.. role:: civ5_color_player_brown_text
.. role:: civ5_color_player_cyan
.. role:: civ5_color_player_cyan_text
.. role:: civ5_color_player_dark_blue
.. role:: civ5_color_player_dark_blue_text
.. role:: civ5_color_player_dark_cyan
.. role:: civ5_color_player_dark_cyan_text
.. role:: civ5_color_player_dark_green
.. role:: civ5_color_player_dark_dark_green
.. role:: civ5_color_player_dark_green_text
.. role:: civ5_color_player_dark_pink
.. role:: civ5_color_player_dark_pink_text
.. role:: civ5_color_player_dark_purple
.. role:: civ5_color_player_dark_purple_text
.. role:: civ5_color_player_dark_red
.. role:: civ5_color_player_dark_red_text
.. role:: civ5_color_player_dark_yellow
.. role:: civ5_color_player_dark_yellow_text
.. role:: civ5_color_player_gray
.. role:: civ5_color_player_gray_text
.. role:: civ5_color_player_green
.. role:: civ5_color_player_green_text
.. role:: civ5_color_player_orange
.. role:: civ5_color_player_orange_text
.. role:: civ5_color_player_peach
.. role:: civ5_color_player_peach_text
.. role:: civ5_color_player_pink
.. role:: civ5_color_player_pink_text
.. role:: civ5_color_player_purple
.. role:: civ5_color_player_purple_text
.. role:: civ5_color_player_red
.. role:: civ5_color_player_red_text
.. role:: civ5_color_player_white
.. role:: civ5_color_player_white_text
.. role:: civ5_color_player_yellow
.. role:: civ5_color_player_yellow_text
.. role:: civ5_color_player_light_green
.. role:: civ5_color_player_light_green_text
.. role:: civ5_color_player_light_blue
.. role:: civ5_color_player_blue_text
.. role:: civ5_color_player_light_yellow
.. role:: civ5_color_player_light_yellow_text
.. role:: civ5_color_player_light_purple
.. role:: civ5_color_player_light_purple_text
.. role:: civ5_color_player_light_orange
.. role:: civ5_color_player_light_orange_text
.. role:: civ5_color_player_middle_purple
.. role:: civ5_color_player_middle_purple_text
.. role:: civ5_color_player_goldenrod
.. role:: civ5_color_player_dark_gray
.. role:: civ5_color_player_dark_gray_text
.. role:: civ5_color_player_middle_green
.. role:: civ5_color_player_middle_green_text
.. role:: civ5_color_player_dark_lemon
.. role:: civ5_color_player_dark_lemon_text
.. role:: civ5_color_player_middle_blue
.. role:: civ5_color_player_middle_blue_text
.. role:: civ5_color_player_middle_cyan
.. role:: civ5_color_player_middle_cyan_text
.. role:: civ5_color_player_maroon
.. role:: civ5_color_player_light_brown
.. role:: civ5_color_player_light_brown_text
.. role:: civ5_color_player_dark_orange
.. role:: civ5_color_player_dark_orange_text
.. role:: civ5_color_player_dark_dark_green_text
.. role:: civ5_color_player_pale_red
.. role:: civ5_color_player_dark_indigo
.. role:: civ5_color_player_dark_indigo_text
.. role:: civ5_color_player_pale_orange
.. role:: civ5_color_player_light_black
.. role:: civ5_color_player_light_black_text
.. role:: civ5_color_player_minor_icon
.. role:: civ5_color_player_barbarian_icon
.. role:: civ5_color_player_america_icon
.. role:: civ5_color_player_arabia_icon
.. role:: civ5_color_player_aztec_icon
.. role:: civ5_color_player_china_icon
.. role:: civ5_color_player_egypt_icon
.. role:: civ5_color_player_england_icon
.. role:: civ5_color_player_france_icon
.. role:: civ5_color_player_germany_icon
.. role:: civ5_color_player_greece_icon
.. role:: civ5_color_player_india_icon
.. role:: civ5_color_player_iroquois_icon
.. role:: civ5_color_player_japan_icon
.. role:: civ5_color_player_ottoman_icon
.. role:: civ5_color_player_persia_icon
.. role:: civ5_color_player_rome_icon
.. role:: civ5_color_player_russia_icon
.. role:: civ5_color_player_siam_icon
.. role:: civ5_color_player_songhai_icon
.. role:: civ5_color_player_barbarian_background
.. role:: civ5_color_player_america_background
.. role:: civ5_color_player_arabia_background
.. role:: civ5_color_player_aztec_background
.. role:: civ5_color_player_china_background
.. role:: civ5_color_player_egypt_background
.. role:: civ5_color_player_england_background
.. role:: civ5_color_player_france_background
.. role:: civ5_color_player_germany_background
.. role:: civ5_color_player_greece_background
.. role:: civ5_color_player_india_background
.. role:: civ5_color_player_iroquois_background
.. role:: civ5_color_player_japan_background
.. role:: civ5_color_player_ottoman_background
.. role:: civ5_color_player_persia_background
.. role:: civ5_color_player_rome_background
.. role:: civ5_color_player_russia_background
.. role:: civ5_color_player_siam_background
.. role:: civ5_color_player_songhai_background
"""


# -- Lua support -------------------------------------------------------------

lua_source_path = [
    "./lua",
]


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'nature'

#  html_theme_options = {
    #  "bgcolor": "#212121",
    #  "textcolor": "#FFFFFF",
    #  "headbgcolor": "#333333",
    #  "headtextcolor": "#FFFFFF",
    #  "headlinkcolor": "#FFFFFF",
#  }

html_show_copyright = False

html_last_updated_fmt = ""

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

html_css_files = ['style/style.css']

html_sidebars = { '**': ['globaltoc.html', 'relations.html', 'sourcelink.html', 'searchbox.html'] }

html_logo = '_static/image/civ5_logo_256.png'
html_favicon = '_static/image/favicon.ico'
