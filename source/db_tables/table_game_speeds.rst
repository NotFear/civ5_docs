GameSpeeds, GameSpeed_Turns
===========================

.. contents:: :local:

.. _civ5_table_game_speeds:

GameSpeeds
----------

TODO: Clean this up

.. code-block:: xml

	<Table name="GameSpeeds">
		<Column name="ID" type="integer" primarykey="true" autoincrement="true"/>
		<Column name="Type" type="text" notnull="true" unique="true"/>
		<Column name="Description" type="text"/>
		<Column name="Help" type="text"/>
		<Column name="DealDuration" type="integer" default="0"/>
		<Column name="GrowthPercent" type="integer" default="0"/>
		<Column name="TrainPercent" type="integer" default="0"/>
		<Column name="ConstructPercent" type="integer" default="0"/>
		<Column name="CreatePercent" type="integer" default="0"/>
		<Column name="ResearchPercent" type="integer" default="0"/>
		<Column name="GoldPercent" type="integer" default="0"/>
		<Column name="GoldGiftMod" type="integer" default="0"/>
		<Column name="BuildPercent" type="integer" default="0"/>
		<Column name="ImprovementPercent" type="integer" default="0"/>
		<Column name="GreatPeoplePercent" type="integer" default="0"/>
		<Column name="CulturePercent" type="integer" default="0"/>
		<Column name="FaithPercent" type="integer" default="0"/>
		<Column name="BarbPercent" type="integer" default="0"/>
		<Column name="FeatureProductionPercent" type="integer" default="0"/>
		<Column name="UnitDiscoverPercent" type="integer" default="0"/>
		<Column name="UnitHurryPercent" type="integer" default="0"/>
		<Column name="UnitTradePercent" type="integer" default="0"/>
		<Column name="GoldenAgePercent" type="integer" default="0"/>
		<Column name="HurryPercent" type="integer" default="0"/>
		<Column name="InflationPercent" type="integer" default="0"/>
		<Column name="InflationOffset" type="integer" default="0"/>
		<Column name="ReligiousPressureAdjacentCity" type="integer" default="0"/>
		<Column name="VictoryDelayPercent" type="integer" default="0"/>
		<Column name="MinorCivElectionFreqMod" type="integer" default="0"/>
		<Column name="OpinionDurationPercent" type="integer" default="0"/>
		<Column name="SpyRatePercent" type="integer" default="0"/>
		<Column name="PeaceDealDuration" type="integer" default="0"/>
		<Column name="RelationshipDuration" type="integer" default="0"/>
		<Column name="LeaguePercent" type="integer" default="0"/>
		<Column name="PortraitIndex" type="integer" default="-1"/>
		<Column name="IconAtlas" type="text" default="NULL" reference="IconTextureAtlases(Atlas)"/>
	</Table>

.. _civ5_table_game_speed_turns:

GameSpeed_Turns
---------------

TODO: Clean this up

.. code-block:: xml

	<Table name="GameSpeed_Turns">
		<Column name="GameSpeedType" type="text" reference="GameSpeeds(Type)"/>
		<Column name="MonthIncrement" type="integer"/>
		<Column name="TurnsPerIncrement" type="integer"/>
	</Table>

Sources
-------

- ``Assets/DLC/Expansion2/Gameplay/XML/GameInfo/CIV5GameSpeeds.xml``
- ``Assets/DLC/Expansion/Gameplay/XML/GameInfo/CIV5GameSpeeds.xml``
- ``Assets/Gameplay/XML/GameInfo/CIV5GameSpeeds.xml``
