#!/usr/bin/env ruby

require "nokogiri"

def main
  abort "Usage: #{$0} path/to/CIV5Colors.xml" if ARGV.length < 1

  content = File.read(ARGV[0])
  xml = Nokogiri::XML(content)
  xml.xpath("/GameData/Colors/Row").each do |elem|
    color_name = elem.at_xpath("Type").content
    red = elem.at_xpath("Red").content.to_f
    green = elem.at_xpath("Green").content.to_f
    blue = elem.at_xpath("Blue").content.to_f
    alpha = elem.at_xpath("Alpha").content.to_f

    brightness = brightness(red * 255, green * 255, blue * 255) / 255.0

    background = if brightness > 0.4
      "background: #000;"
    else
      "background: #FFF;"
    end

    puts ".civ5-#{color_name.downcase.gsub("_", "-")} { #{background} color: rgba(#{(red * 255).floor}, #{(green * 255).floor}, #{(blue * 255).floor}, #{alpha}); }"
  end
end

def brightness(red, green, blue)
 Math.sqrt(
   0.299 * (red ** 2) +
   0.587 * (green ** 2) +
   0.114 * (blue ** 2)
 ).to_i
end

if __FILE__ == $0
  main
end
