The sphinx-lua repo is over at https://github.com/boolangery/sphinx-lua
The py-lua-doc repo is over at https://github.com/boolangery/py-lua-doc

This repo contains some modifications to those projects, to better format the docs (in my opinion).
