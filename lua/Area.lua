--- @class Area
local Area = {}

--- @return bool
function Area:IsNone() end

--- @return int
function Area:CalculateTotalBestNatureYield() end

--- @return int
function Area:CountCoastalLand() end

--- @return int
function Area:CountNumUniqueResourceTypes() end

--- @return int
function Area:GetID() end

--- @return int
function Area:GetNumTiles() end

--- @return bool
-- Commented out in C++
-- function Area:IsLake() end

--- @return int
function Area:GetNumOwnedTiles() end

--- @return int
function Area:GetNumUnownedTiles() end

--- @return int
function Area:GetNumRiverEdges() end

--- @return int
function Area:GetNumCities() end

--- @return int
function Area:GetNumUnits() end

--- @return int
function Area:GetTotalPopulation() end

--- @return int
function Area:GetNumStartingPlots() end

--- @return bool
function Area:IsWater() end

--- @param player_id PlayerTypes
---
--- @return int
function Area:GetUnitsPerPlayer(player_id) end

--- @param player_id PlayerTypes
---
--- @return int
function Area:GetCitiesPerPlayer(player_id) end

--- @param player_id PlayerTypes
---
--- @return int
function Area:GetPopulationPerPlayer(player_id) end

--- @param player_id PlayerTypes
---
--- @return int
function Area:GetFreeSpecialist(player_id) end

--- @param team_id TeamTypes
---
--- @return int
function Area:GetNumRevealedTiles(team_id) end

--- @param team_id TeamTypes
---
--- @return int
function Area:GetNumUnrevealedTiles(team_id) end

--- @param player_id PlayerTypes
---
--- @return int
function Area:GetTargetCity(player_id) end

--- @param player_id PlayerTypes
--- @param yield_type YieldTypes
---
--- @return int
function Area:GetYieldRateModifier(player_id, yield_type) end

--- @param resource_type ResourceTypes
---
--- @return int
function Area:GetNumResources(resource_type) end

--- @return int
function Area:GetNumTotalResources() end

--- @param improvement_type ImprovementTypes
---
--- @return int
function Area:GetNumImprovements(improvement_type) end

return Area
