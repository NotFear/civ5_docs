Projects, Project_Flavors, Project_Prereqs, Project_VictoryThresholds, Project_ResourceQuantityRequirements
===========================================================================================================

.. contents:: :local:

.. _civ5_table_projects:

Projects
--------

.. col2-nowrap seems to break things a little (the column being narrower than its contents) and I don't know enough
   about css to fix it ;P

.. list-table::
   :header-rows: 1
   :widths: 1 15 20 100
   :class: col1-nowrap col3-nowrap

   * - Name
     - Type
     - Options
     - Description

   * - ID
     - |icon_int| Integer
     - | Primary Key
       | Autoincrement
     - Unique identifier for the project

   * - Type
     - |icon_string| Text
     - | Not Null
       | Unique
     - Unique name for the project

       Example: ``PROJECT_MANHATTAN_PROJECT``

   * - Description
     - |icon_string| Text
     -
     - Localization key for the project's name

       Example: ``TXT_KEY_PROJECT_MANHATTAN_PROJECT`` (which translates into ``Manhattan Project``)

   * - Civilopedia
     - |icon_string| Text
     -
     - Localization key for the project's historical info, displayed in the Civilopedia

       Example: ``TXT_KEY_PROJECT_MANHATTAN_PROJECT_PEDIA`` (which translates into ``The Manhattan Project is the code
       name for the Anglo-European effort...``)

   * - Strategy
     - |icon_string| Text
     -
     - Localization key for the project's strategy text, displayed in the Civilopedia

       Example: ``TXT_KEY_PROJECT_MANHATTAN_PROJECT_STRATEGY`` (which translates into ``The Manhattan Project allows a
       civilization to construct nuclear weapons. Each civilization must construct the Manhattan Project before it can
       construct nukes.``)

   * - Help
     - |icon_string| Text
     -
     - Localization key for the project's help text, displayed as a tooltip and also in the Civilopedia

       Example: ``TXT_KEY_PROJECT_MANHATTAN_PROJECT_HELP`` (which translates into ``Allows your Cities to build Atomic
       Bombs and Nuclear Missiles.``)

   * - Requirements
     - |icon_string| Text
     -
     - TODO: What is this?

   * - MaxGlobalInstances
     - |icon_int| Integer
     - Default 0
     - The maximum number of times a project can be constructed in the world. Set to ``-1`` to make this unlimited.
       International wonders generally have this set to ``1``.

   * - MaxTeamInstances
     - |icon_int| Integer
     - Default 0
     - The maximum number of times a project can be constructed in per-team. Set to ``-1`` to make this unlimited.

   * - Cost
     - |icon_int| Integer
     - Default 0
     - The base production cost of the project. The actual cost will vary depending on :ref:`game speeds
       <civ5_table_game_speeds>`.

   * - NukeInterception
     - |icon_int| Integer
     - Default 0
     - TODO: What is this?

   * - CultureBranchesRequired
     - |icon_int| Integer
     - Default 0
     - TODO: What is this?

   * - TechShare
     - |icon_int| Integer
     - Default 0
     - TODO: What is this?

   * - VictoryDelayPercent
     - |icon_int| Integer
     - Default 0
     - TODO: What is this?

   * - Spaceship
     - |icon_bool| Boolean
     - Default 0
     - ``1`` for spaceship parts, ``0`` otherwise

   * - Religious
     - |icon_bool| Boolean
     - Default 0
     - TODO: What is this?

   * - AllowsNukes
     - |icon_bool| Boolean
     - Default 0
     - Whether complelting this project show allow access to nukes

       Set to ``1`` for the Manhattan Project.

   * - MovieDefineTag
     - |icon_string| Text
     - Default Null
     - TODO: What is this?

   * - VictoryPrereq
     - |icon_string| Text
     - Default Null
     - What victories this is a prerequisite for

       Example: ``VICTORY_SPACE_RACE`` is set for all spaceship parts

   * - TechPrereq
     - |icon_string| Text
     - Default Null
     - What prerequisite technology need to be unlocked before this project is available

       Example: ``TECH_ATOMIC_THEORY`` is set for the Manhattan Project

   * - EveryoneSpecialUnit
     - |icon_string| Text
     - Default Null
     - TODO: What is this?

   * - CreateSound
     - |icon_string| Text
     - Default Null
     - TODO: What is this?

   * - AnyonePrereqProject
     - |icon_string| Text
     - Default Null
     - TODO: What is this?

   * - PortraitIndex
     - |icon_int| Integer
     - Default -1
     - TODO: What is this?

   * - IconAtlas
     - |icon_string| Text
     - | Default Null
       | References :ref:`civ5_table_icon_texture_atlases`\ (Atlas)
     - TODO: What is this?

       Example: ``BW_ATLAS_2``

.. _civ5_table_project_flavors:

Project_Flavors
---------------

Flavors help the game AI decide how it wants to play. Different leaders have different biases towards various flavors
(found in the :ref:`civ5_table_leader_flavors` table), and then various projects have weightings towards the various
flavors. As an example, a leader with a high ``FLAVOR_NUKE`` flavor value is likely to build the Manhattan Project soon
after they unlock it, since ``PROJECT_MANHATTAN_PROJECT`` has a high ``FLAVOR_NUKE`` rating of ``250``.

.. list-table::
   :header-rows: 1
   :widths: 1 15 20 100
   :class: col1-nowrap col3-nowrap

   * - Name
     - Type
     - Options
     - Description

   * - ProjectType
     - |icon_string| Text
     - References :ref:`civ5_table_projects`\ (Type)
     - The project we want to add a flavor to

       Example: ``PROJECT_APOLLO_PROGRAM``

   * - FlavorType
     - |icon_string| Text
     - References :ref:`civ5_table_flavors`\ (Type)
     - The flavor to add

       Example: ``FLAVOR_SPACESHIP``

   * - Flavor
     - |icon_int| Integer
     -
     - Weighting factor AI decision making

       Example: ``250``

.. _civ5_table_project_prereqs:

Project_Prereqs
---------------

.. list-table::
   :header-rows: 1
   :widths: 1 15 20 100
   :class: col1-nowrap col3-nowrap

   * - Name
     - Type
     - Options
     - Description

   * - ProjectType
     - |icon_string| Text
     - References :ref:`civ5_table_projects`\ (Type)
     - The project we want to add a prerequisite project to

       Example: ``PROJECT_SS_BOOSTER``

   * - PrereqProjectType
     - |icon_string| Text
     - References :ref:`civ5_table_projects`\ (Type)
     - The prerequisite project needed for the given ``ProjectType``

       Example: ``PROJECT_APOLLO_PROGRAM``

   * - AmountNeeded
     - |icon_int| Integer
     - Default 1
     - How many times the prerequisite project needs to have been constructed to unlock the given ``ProjectType``

       Example: ``1``

.. _civ5_table_project_victory_thresholds:

Project_VictoryThresholds
-------------------------

.. list-table::
   :header-rows: 1
   :widths: 1 15 20 100
   :class: col1-nowrap col3-nowrap

   * - Name
     - Type
     - Options
     - Description

   * - ProjectType
     - |icon_string| Text
     - References :ref:`civ5_table_projects`\ (Type)
     - The project we want to add a threshold to

       Example: ``PROJECT_SS_BOOSTER``

   * - VictoryType
     - |icon_string| Text
     - References :ref:`civ5_table_victories`\ (Type)
     - The victory the project achieves

       Example: ``VICTORY_SPACE_RACE``

   * - Threshold
     - |icon_int| Integer
     - Not Null
     - TODO: What is this? And how does it differ from ``MinThreshold``?

       Example: ``3``

   * - MinThreshold
     - |icon_int| Integer
     - Default 0
     - TODO: What is this? And how does it differ from ``Threshold``?

       Example: ``3``

.. _civ5_table_project_resource_quantity_requirements:

Project_ResourceQuantityRequirements
------------------------------------

.. list-table::
   :header-rows: 1
   :widths: 1 15 20 100
   :class: col1-nowrap col3-nowrap

   * - Name
     - Type
     - Options
     - Description

   * - ProjectType
     - |icon_string| Text
     - References :ref:`civ5_table_projects`\ (Type)
     - TODO: What does this do?

   * - ResourceType
     - |icon_string| Text
     - References :ref:`civ5_table_resources`\ (Type)
     - TODO: What does this do?

   * - Threshold
     - |icon_int| Integer
     - Not Null
     - TODO: What does this do?

Sources
-------

- ``Assets/Gameplay/XML/GameInfo/CIV5Projects.xml``
- ``Assets/DLC/Expansion2/Gameplay/XML/GameInfo/CIV5Projects.xml``
