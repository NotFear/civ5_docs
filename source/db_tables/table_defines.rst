Defines, PostDefines
====================

.. contents:: :local:

.. _civ5_table_defines:

Defines
-------

Various game constants are defined in the ``Defines`` table.

.. col2-nowrap seems to break things a little (the column being narrower than its contents) and I don't know enough
   about css to fix it ;P

.. list-table::
   :header-rows: 1
   :widths: 1 15 20 100
   :class: col1-nowrap col3-nowrap

   * - Name
     - Type
     - Options
     - Description

   * - Name
     - |icon_string| Text
     - Primary Key
     - Key

       Example: ``CITY_ATTACK_RANGE``

   * - Value
     - |icon_variant| Variant
     -
     - Value

       Example: ``2``

.. _civ5_table_post_defines:

PostDefines
-----------

Post defines are references to rows in tables after everything has been loaded. The ``Type`` of the post define and the
rowid it points to is added to :ref:`civ5_table_defines`.

For example, specifying ``Name='SPACE_RACE_TRIGGER_PROJECT', Key='PROJECT_APOLLO_PROGRAM', Table='Projects'``, where
``Type='PROJECT_APOLLO_PROGRAM', ID=1`` exists in the ``Projects`` table, will cause the game to add
``Name='SPACE_RACE_TRIGGER_PROJECT', Value=1`` into the :ref:`civ5_table_defines` table.

.. list-table::
   :header-rows: 1
   :widths: 1 15 20 100
   :class: col1-nowrap col3-nowrap

   * - Name
     - Type
     - Options
     - Description

   * - Name
     - |icon_string| Text
     - Primary Key
     - Key

       Example: ``SPACE_RACE_TRIGGER_PROJECT``

   * - Key
     - |icon_string| Text
     -
     - The key to fetch the rowid for in a foreign table

       The foreign table must have a ``Type`` column, as that column is looked up to find a matching key.

       Example: ``PROJECT_APOLLO_PROGRAM``

   * - Table
     - |icon_string| Text
     -
     - The table that the ``Key`` lives in

       Example: ``Projects``

List of defines
---------------

.. list-table::
   :header-rows: 1
   :widths: 1 1 100

   * - Name
     - Default value
     - Description

   * - CAN_WORK_WATER_FROM_GAME_START
     - 1
     -
   * - NAVAL_PLOT_BLOCKADE_RANGE
     - 2
     -
   * - EVENT_MESSAGE_TIME
     - 10
     -
   * - START_YEAR
     - -4000
     - What year the game starts in. ``-4000`` means "4000 BC".
   * - WEEKS_PER_MONTHS
     - 4
     -
   * - HIDDEN_START_TURN_OFFSET
     - 0
     -
   * - RECON_VISIBILITY_RANGE
     - 6
     -
   * - PLOT_VISIBILITY_RANGE
     - 1
     -
   * - UNIT_VISIBILITY_RANGE
     - 1
     -
   * - AIR_UNIT_REBASE_RANGE_MULTIPLIER
     - 200
     -
   * - MOUNTAIN_SEE_FROM_CHANGE
     - 2
     -
   * - MOUNTAIN_SEE_THROUGH_CHANGE
     - 2
     -
   * - HILLS_SEE_FROM_CHANGE
     - 1
     -
   * - HILLS_SEE_THROUGH_CHANGE
     - 1
     -
   * - SEAWATER_SEE_FROM_CHANGE
     - 1
     -
   * - SEAWATER_SEE_THROUGH_CHANGE
     - 1
     -
   * - MAX_YIELD_STACK
     - 5
     -
   * - MOVE_DENOMINATOR
     - 60
     -
   * - STARTING_DISTANCE_PERCENT
     - 12
     -
   * - MIN_CIV_STARTING_DISTANCE
     - 10
     -
   * - MIN_CITY_RANGE
     - 3
     -
   * - OWNERSHIP_SCORE_DURATION_THRESHOLD
     - 20
     -
   * - NUM_POLICY_BRANCHES_ALLOWED
     - 2
     -
   * - VICTORY_POINTS_NEEDED_TO_WIN
     - 20
     -
   * - NUM_VICTORY_POINT_AWARDS
     - 5
     -
   * - NUM_OR_TECH_PREREQS
     - 3
     -
   * - NUM_AND_TECH_PREREQS
     - 6
     -
   * - NUM_UNIT_AND_TECH_PREREQS
     - 3
     -
   * - NUM_BUILDING_AND_TECH_PREREQS
     - 3
     -
   * - NUM_BUILDING_RESOURCE_PREREQS
     - 5
     -
   * - BASE_RESEARCH_RATE
     - 1
     -
   * - MAX_WORLD_WONDERS_PER_CITY
     - -1
     - The maximum number of world wonders that can be built per-city. ``-1`` means unlimited.

       The max world wonder count is always unlimited in "One City Challenge" mode.

       See :lua:meth:`City.IsWorldWondersMaxed`
   * - MAX_TEAM_WONDERS_PER_CITY
     - -1
     - The maximum number of team wonders that can be built per-city. ``-1`` means unlimited.

       The max team wonder count is always unlimited in "One City Challenge" mode.

       See :lua:meth:`City.IsTeamWondersMaxed`
   * - MAX_NATIONAL_WONDERS_PER_CITY
     - -1
     - The maximum number of national wonders that can be built per-city. ``-1`` means unlimited.

       The max national wonders for "One City Challenge" mode has a different define,
       ``MAX_NATIONAL_WONDERS_PER_CITY_FOR_OCC``.

       See :lua:meth:`City.IsNationalWondersMaxed`
   * - MAX_NATIONAL_WONDERS_PER_CITY_FOR_OCC
     - -1
     - The maximum number of national wonders that can be built per-city in "One City Challenge" mode. ``-1`` means
       unlimited.

       See :lua:meth:`City.IsNationalWondersMaxed`
   * - MAX_BUILDINGS_PER_CITY
     - -1
     - The maximum number of buildings that can be built per-city. ``-1`` means unlimited.

       The max building count is always unlimited in "One City Challenge" mode.

       See :lua:meth:`City.IsBuildingsMaxed`
   * - INITIAL_CITY_POPULATION
     - 1
     -
   * - BASE_CITY_AIR_STACKING
     - 6
     -
   * - CITY_CAPTURE_POPULATION_PERCENT
     - 50
     -
   * - BASE_CITY_GROWTH_THRESHOLD
     - 15
     -
   * - CITY_GROWTH_MULTIPLIER
     - 8
     -
   * - CITY_GROWTH_EXPONENT
     - 1.5
     -
   * - FOOD_CONSUMPTION_PER_POPULATION
     - 2
     -
   * - HAPPINESS_PER_CITY_WITH_STATE_RELIGION
     - 0
     -
   * - HAPPINESS_PER_NATURAL_WONDER
     - 1
     -
   * - HAPPINESS_PER_EXTRA_LUXURY
     - 0
     -
   * - UNHAPPINESS_PER_POPULATION
     - 1
     -
   * - UNHAPPINESS_PER_OCCUPIED_POPULATION
     - 1.34
     -
   * - UNHAPPINESS_PER_CITY
     - 3
     -
   * - UNHAPPINESS_PER_CAPTURED_CITY
     - 5
     -
   * - UNHAPPY_GROWTH_PENALTY
     - -75
     -
   * - VERY_UNHAPPY_GROWTH_PENALTY
     - -100
     -
   * - VERY_UNHAPPY_CANT_TRAIN_SETTLERS
     - 1
     -
   * - VERY_UNHAPPY_THRESHOLD
     - -10
     -
   * - VERY_UNHAPPY_COMBAT_PENALTY_PER_UNHAPPY
     - -2
     -
   * - VERY_UNHAPPY_MAX_COMBAT_PENALTY
     - -40
     -
   * - STRATEGIC_RESOURCE_EXHAUSTED_PENALTY
     - -50
     -
   * - VERY_UNHAPPY_PRODUCTION_PENALTY_PER_UNHAPPY
     - -2
     -
   * - VERY_UNHAPPY_MAX_PRODUCTION_PENALTY
     - -40
     -
   * - VERY_UNHAPPY_GOLD_PENALTY_PER_UNHAPPY
     - -2
     -
   * - VERY_UNHAPPY_MAX_GOLD_PENALTY
     - -40
     -
   * - SUPER_UNHAPPY_THRESHOLD
     - -20
     -
   * - UPRISING_COUNTER_MIN
     - 4
     -
   * - UPRISING_COUNTER_POSSIBLE
     - 3
     -
   * - UPRISING_NUM_BASE
     - 100
     -
   * - UPRISING_NUM_CITY_COUNT
     - 20
     -
   * - REVOLT_COUNTER_MIN
     - 5
     -
   * - WLTKD_GROWTH_MULTIPLIER
     - 25
     -
   * - INDUSTRIAL_ROUTE_PRODUCTION_MOD
     - 25
     -
   * - RESOURCE_DEMAND_COUNTDOWN_BASE
     - 15
     -
   * - RESOURCE_DEMAND_COUNTDOWN_CAPITAL_ADD
     - 25
     -
   * - RESOURCE_DEMAND_COUNTDOWN_RAND
     - 10
     -
   * - NEW_HURRY_MODIFIER
     - 50
     -
   * - GREAT_GENERAL_RANGE
     - 2
     -
   * - GREAT_GENERAL_STRENGTH_MOD
     - 15
     -
   * - BONUS_PER_ADJACENT_FRIEND
     - 10
     -
   * - POLICY_ATTACK_BONUS_MOD
     - 25
     -
   * - CONSCRIPT_MIN_CITY_POPULATION
     - 5
     -
   * - CONSCRIPT_POPULATION_PER_COST
     - 60
     -
   * - MIN_TIMER_UNIT_DOUBLE_MOVES
     - 32
     -
   * - COMBAT_DAMAGE
     - 20
     -
   * - NONCOMBAT_UNIT_RANGED_DAMAGE
     - 40
     -
   * - NAVAL_COMBAT_DEFENDER_STRENGTH_MULTIPLIER
     - 100
     -
   * - QUICKSAVE
     - QuickSave
     - Appears unused
   * - LAKE_PLOT_RAND
     - 160
     -
   * - PLOTS_PER_RIVER_EDGE
     - 12
     -
   * - RIVER_SOURCE_MIN_RIVER_RANGE
     - 4
     -
   * - RIVER_SOURCE_MIN_SEAWATER_RANGE
     - 2
     -
   * - LAKE_MAX_AREA_SIZE
     - 9
     -
   * - INITIAL_GOLD_PER_UNIT_TIMES_100
     - 50
     -
   * - INITIAL_FREE_OUTSIDE_UNITS
     - 3
     -
   * - INITIAL_OUTSIDE_UNIT_GOLD_PERCENT
     - 0
     -
   * - UNIT_MAINTENANCE_GAME_MULTIPLIER
     - 8
     -
   * - UNIT_MAINTENANCE_GAME_EXPONENT_DIVISOR
     - 7
     -
   * - FREE_UNIT_HAPPINESS
     - 0
     -
   * - TRADE_ROUTE_BASE_GOLD
     - -100
     -
   * - TRADE_ROUTE_CAPITAL_POP_GOLD_MULTIPLIER
     - 15
     -
   * - TRADE_ROUTE_CITY_POP_GOLD_MULTIPLIER
     - 110
     -
   * - DEFICIT_UNIT_DISBANDING_THRESHOLD
     - -5
     -
   * - BUILDING_SALE_DIVISOR
     - 4
     -
   * - DISBAND_UNIT_REFUND_PERCENT
     - 25
     -
   * - GOLDEN_AGE_BASE_THRESHOLD_HAPPINESS
     - 500
     -
   * - GOLDEN_AGE_THRESHOLD_CITY_MULTIPLIER
     - 0.01
     -
   * - GOLDEN_AGE_EACH_GA_ADDITIONAL_HAPPINESS
     - 250
     -
   * - GOLDEN_AGE_VISIBLE_THRESHOLD_DIVISOR
     - 5
     -
   * - BASE_GOLDEN_AGE_UNITS
     - 1
     -
   * - GOLDEN_AGE_UNITS_MULTIPLIER
     - 1
     -
   * - GOLDEN_AGE_LENGTH
     - 10
     - The base number of turns a golden age lasts for, before any game speed modifiers or building modifiers are added.
   * - GOLDEN_AGE_GREAT_PEOPLE_MODIFIER
     - 100
     -
   * - MIN_UNIT_GOLDEN_AGE_TURNS
     - 3
     -
   * - GOLDEN_AGE_CULTURE_MODIFIER
     - 20
     -
   * - HILLS_EXTRA_MOVEMENT
     - 1
     -
   * - RIVER_EXTRA_MOVEMENT
     - 10
     -
   * - FEATURE_GROWTH_MODIFIER
     - 25
     -
   * - ROUTE_FEATURE_GROWTH_MODIFIER
     - -50
     -
   * - EXTRA_YIELD
     - 1
     -
   * - FORTIFY_MODIFIER_PER_TURN
     - 20
     -
   * - MAX_FORTIFY_TURNS
     - 2
     -
   * - NUKE_FALLOUT_PROB
     - 50
     -
   * - NUKE_UNIT_DAMAGE_BASE
     - 30
     -
   * - NUKE_UNIT_DAMAGE_RAND_1
     - 40
     -
   * - NUKE_UNIT_DAMAGE_RAND_2
     - 40
     -
   * - NUKE_NON_COMBAT_DEATH_THRESHOLD
     - 6
     -
   * - NUKE_LEVEL1_POPULATION_DEATH_BASE
     - 30
     -
   * - NUKE_LEVEL1_POPULATION_DEATH_RAND_1
     - 20
     -
   * - NUKE_LEVEL1_POPULATION_DEATH_RAND_2
     - 20
     -
   * - NUKE_LEVEL2_POPULATION_DEATH_BASE
     - 60
     -
   * - NUKE_LEVEL2_POPULATION_DEATH_RAND_1
     - 10
     -
   * - NUKE_LEVEL2_POPULATION_DEATH_RAND_2
     - 10
     -
   * - NUKE_LEVEL2_ELIM_POPULATION_THRESHOLD
     - 5
     -
   * - NUKE_CITY_HIT_POINT_DAMAGE
     - 50
     -
   * - NUKE_BLAST_RADIUS
     - 2
     -
   * - TECH_COST_EXTRA_TEAM_MEMBER_MODIFIER
     - 50
     -
   * - TECH_COST_TOTAL_KNOWN_TEAM_MODIFIER
     - 30
     -
   * - TECH_COST_KNOWN_PREREQ_MODIFIER
     - 20
     -
   * - PEACE_TREATY_LENGTH
     - 10
     -
   * - COOP_WAR_LOCKED_LENGTH
     - 15
     -
   * - BASE_FEATURE_PRODUCTION_PERCENT
     - 67
     -
   * - FEATURE_PRODUCTION_PERCENT_MULTIPLIER
     - 0
     -
   * - DIFFERENT_TEAM_FEATURE_PRODUCTION_PERCENT
     - 67
     -
   * - DEFAULT_WAR_VALUE_FOR_UNIT
     - 100
     -
   * - UNIT_PRODUCTION_PERCENT
     - 100
     -
   * - MAX_UNIT_SUPPLY_PRODMOD
     - 70
     -
   * - BUILDING_PRODUCTION_PERCENT
     - 100
     -
   * - PROJECT_PRODUCTION_PERCENT
     - 100
     -
   * - MAXED_UNIT_GOLD_PERCENT
     - 100
     -
   * - MAXED_BUILDING_GOLD_PERCENT
     - 100
     -
   * - MAXED_PROJECT_GOLD_PERCENT
     - 300
     -
   * - MAX_CITY_DEFENSE_DAMAGE
     - 100
     -
   * - CIRCUMNAVIGATE_FREE_MOVES
     - 0
     -
   * - BASE_CAPTURE_GOLD
     - 20
     -
   * - CAPTURE_GOLD_PER_POPULATION
     - 10
     -
   * - CAPTURE_GOLD_RAND1
     - 20
     -
   * - CAPTURE_GOLD_RAND2
     - 20
     -
   * - CAPTURE_GOLD_MAX_TURNS
     - 50
     -
   * - BARBARIAN_CITY_ATTACK_MODIFIER
     - 0
     -
   * - BUILDING_PRODUCTION_DECAY_TIME
     - 50
     -
   * - BUILDING_PRODUCTION_DECAY_PERCENT
     - 99
     -
   * - UNIT_PRODUCTION_DECAY_TIME
     - 10
     -
   * - UNIT_PRODUCTION_DECAY_PERCENT
     - 98
     -
   * - BASE_UNIT_UPGRADE_COST
     - 10
     -
   * - UNIT_UPGRADE_COST_PER_PRODUCTION
     - 2
     -
   * - UNIT_UPGRADE_COST_MULTIPLIER_PER_ERA
     - 0.0
     -
   * - UNIT_UPGRADE_COST_EXPONENT
     - 1.0
     -
   * - UNIT_UPGRADE_COST_VISIBLE_DIVISOR
     - 5
     -
   * - UNIT_UPGRADE_COST_DISCOUNT_MAX
     - -75
     -
   * - WAR_SUCCESS_UNIT_CAPTURING
     - 1
     -
   * - WAR_SUCCESS_CITY_CAPTURING
     - 10
     -
   * - DIPLO_VOTE_SECRETARY_GENERAL_INTERVAL
     - 4
     -
   * - TEAM_VOTE_MIN_CANDIDATES
     - 2
     -
   * - RESEARCH_AGREEMENT_TIMER
     - 20
     -
   * - RESEARCH_AGREEMENT_BOOST_DIVISOR
     - 3
     -
   * - SCORE_POPULATION_FACTOR
     - 5000
     - Score you get if you have the 'maximum' population possible on the current map
   * - SCORE_LAND_FACTOR
     - 2000
     - Score you get if you have all the land on the current map
   * - SCORE_WONDER_FACTOR
     - 1000
     - Score you get if you have all the wonders
   * - SCORE_TECH_FACTOR
     - 2000
     - Score you get if you have all the techs
   * - SCORE_FREE_PERCENT
     - 0
     - Percentage of the maximum score you get for free
   * - SCORE_VICTORY_PERCENT
     - 0
     - Percentage of your score that gets added if you win the game
   * - SCORE_HANDICAP_PERCENT_OFFSET
     - -60
     - Offset to the score handicap modifier
   * - SCORE_HANDICAP_PERCENT_PER
     - 20
     - The amount of the score modifier per handicap level
   * - MINIMAP_RENDER_SIZE
     - 512
     - Minimap size in pixels
   * - MAX_INTERCEPTION_PROBABILITY
     - 100
     -
   * - MAX_EVASION_PROBABILITY
     - 90
     -
   * - PLAYER_ALWAYS_RAZES_CITIES
     - 0
     -
   * - MIN_WATER_SIZE_FOR_OCEAN
     - 10
     -
   * - CITY_SCREEN_CLICK_WILL_EXIT
     - 0
     -
   * - WATER_POTENTIAL_CITY_WORK_FOR_AREA
     - 0
     -
   * - LAND_UNITS_CAN_ATTACK_WATER_CITIES
     - 0
     -
   * - CITY_MAX_NUM_BUILDINGS
     - 1
     -
   * - CITY_MIN_SIZE_FOR_SETTLERS
     - 2
     -
   * - RANGED_ATTACKS_USE_MOVES
     - 0
     -
   * - ADVANCED_START_ALLOW_UNITS_OUTSIDE_CITIES
     - 0
     -
   * - ADVANCED_START_MAX_UNITS_PER_CITY
     - 2
     -
   * - ADVANCED_START_CITY_COST
     - 84
     -
   * - ADVANCED_START_CITY_COST_INCREASE
     - 0
     -
   * - ADVANCED_START_POPULATION_COST
     - 150
     -
   * - ADVANCED_START_POPULATION_COST_INCREASE
     - 0
     -
   * - ADVANCED_START_VISIBILITY_COST
     - 2
     -
   * - ADVANCED_START_VISIBILITY_COST_INCREASE
     - 3
     -
   * - ADVANCED_START_CITY_PLACEMENT_MAX_RANGE
     - 6
     -
   * - NEW_CITY_BUILDING_VALUE_MODIFIER
     - -60
     -
   * - PATH_DAMAGE_WEIGHT
     - 0
     -
   * - PUPPET_SCIENCE_MODIFIER
     - -25
     -
   * - PUPPET_CULTURE_MODIFIER
     - -25
     -
   * - PUPPET_GOLD_MODIFIER
     - 0
     -
   * - PUPPET_FAITH_MODIFIER
     - 0
     -
   * - BASE_POLICY_COST
     - 25
     - | Policy Cost
       |
       | Firaxis comment says ``Mod for city count is in Worlds.xml`` - ``Worlds.xml`` doesn't seem to exist though
   * - POLICY_COST_INCREASE_TO_BE_EXPONENTED
     - 3
     -
   * - POLICY_COST_EXPONENT
     - 2.01
     -
   * - POLICY_COST_VISIBLE_DIVISOR
     - 5
     -
   * - SWITCH_POLICY_BRANCHES_ANARCHY_TURNS
     - 2
     -
   * - SWITCH_POLICY_BRANCHES_TENETS_LOST
     - 2
     -
   * - POLICY_COST_DISCOUNT_MAX
     - -75
     -
   * - GOLD_PURCHASE_GOLD_PER_PRODUCTION
     - 30
     -
   * - HURRY_GOLD_PRODUCTION_EXPONENT
     - 0.75
     -
   * - GOLD_PURCHASE_VISIBLE_DIVISOR
     - 10
     - When purchasing items in a city with gold or faith, the costs of each item will be rounded down to a multiple of
       this value.
   * - PROJECT_PURCHASING_DISABLED
     - 1
     -
   * - HURRY_GOLD_TECH_EXPONENT
     - 1.10
     -
   * - HURRY_GOLD_CULTURE_EXPONENT
     - 1.10
     -
   * - INFLUENCE_MOUNTAIN_COST
     - 3
     -
   * - INFLUENCE_HILL_COST
     - 1
     -
   * - INFLUENCE_RIVER_COST
     - 1
     -
   * - USE_FIRST_RING_INFLUENCE_TERRAIN_COST
     - 0
     -
   * - NUM_RESOURCE_QUANTITY_TYPES
     - 4
     -
   * - SPECIALISTS_DIVERT_POPULATION_ENABLED
     - 0
     -
   * - SCIENCE_PER_POPULATION
     - 1
     -
   * - RESEARCH_AGREEMENT_MOD
     - 0
     -
   * - BARBARIAN_CAMP_FIRST_TURN_PERCENT_OF_TARGET_TO_ADD
     - 33
     -
   * - BARBARIAN_CAMP_ODDS_OF_NEW_CAMP_SPAWNING
     - 2
     -
   * - BARBARIAN_CAMP_MINIMUM_DISTANCE_CAPITAL
     - 4
     -
   * - BARBARIAN_CAMP_MINIMUM_DISTANCE_ANOTHER_CAMP
     - 7
     -
   * - BARBARIAN_CAMP_COASTAL_SPAWN_ROLL
     - 6
     -
   * - BARBARIAN_EXTRA_RAGING_UNIT_SPAWN_CHANCE
     - 10
     -
   * - BARBARIAN_NAVAL_UNIT_START_TURN_SPAWN
     - 30
     -
   * - MAX_BARBARIANS_FROM_CAMP_NEARBY
     - 2
     -
   * - MAX_BARBARIANS_FROM_CAMP_NEARBY_RANGE
     - 4
     -
   * - GOLD_FROM_BARBARIAN_CONVERSION
     - 25
     -
   * - BARBARIAN_CITY_GOLD_RANSOM
     - 200
     -
   * - BARBARIAN_UNIT_GOLD_RANSOM
     - 100
     -
   * - EMBARKED_UNIT_MOVEMENT
     - 2
     -
   * - EMBARKED_VISIBILITY_RANGE
     - 0
     -
   * - DEFAULT_MAX_NUM_BUILDERS
     - -1
     -
   * - BARBARIAN_TECH_PERCENT
     - 75
     -
   * - CITY_RESOURCE_WLTKD_TURNS
     - 20
     -
   * - MAX_SPECIALISTS_FROM_BUILDING
     - 4
     -
   * - GREAT_PERSON_THRESHOLD_BASE
     - 100
     -
   * - GREAT_PERSON_THRESHOLD_INCREASE
     - 100
     -
   * - CULTURE_BOMB_COOLDOWN
     - 10
     -
   * - CULTURE_BOMB_MINOR_FRIENDSHIP_CHANGE
     - -50
     -
   * - LANDMARK_MINOR_FRIENDSHIP_CHANGE
     - 50
     -
   * - POST_COMBAT_TEXT_DELAY
     - 1.0
     -
   * - UNIT_AUTO_EXPLORE_DISABLED
     - 0
     - Is auto-explore disabled for units? (but not hidden)
   * - UNIT_AUTO_EXPLORE_FULL_DISABLED
     - 0
     - Is auto-explore disabled for units? (and hidden)
   * - UNIT_WORKER_AUTOMATION_DISABLED
     - 0
     - Is worker automation disabled for units? (but the button not hidden)
   * - UNIT_DELETE_DISABLED
     - 0
     - Are units allowed to blow themselves up? (but the button not hidden)
   * - MIN_START_AREA_TILES
     - 4
     - Related to starting location
   * - MIN_DISTANCE_OTHER_AREA_PERCENT
     - 75
     - Related to starting location
   * - MINOR_CIV_FOOD_REQUIREMENT
     - 2
     -
   * - MAJOR_CIV_FOOD_REQUIREMENT
     - 2
     -
   * - MIN_START_FOUND_VALUE_AS_PERCENT_OF_BEST
     - 50
     -
   * - START_AREA_FOOD_MULTIPLIER
     - 6
     -
   * - START_AREA_HAPPINESS_MULTIPLIER
     - 12
     -
   * - START_AREA_PRODUCTION_MULTIPLIER
     - 8
     -
   * - START_AREA_GOLD_MULTIPLIER
     - 2
     -
   * - START_AREA_SCIENCE_MULTIPLIER
     - 1
     -
   * - START_AREA_FAITH_MULTIPLIER
     - 1
     -
   * - START_AREA_RESOURCE_MULTIPLIER
     - 1
     -
   * - START_AREA_STRATEGIC_MULTIPLIER
     - 1
     -
   * - START_AREA_BUILD_ON_COAST_PERCENT
     - 20
     -
   * - SETTLER_FOOD_MULTIPLIER
     - 15
     -
   * - SETTLER_HAPPINESS_MULTIPLIER
     - 8
     -
   * - SETTLER_PRODUCTION_MULTIPLIER
     - 3
     -
   * - SETTLER_GOLD_MULTIPLIER
     - 3
     -
   * - SETTLER_SCIENCE_MULTIPLIER
     - 1
     -
   * - SETTLER_FAITH_MULTIPLIER
     - 1
     -
   * - SETTLER_RESOURCE_MULTIPLIER
     - 3
     -
   * - SETTLER_STRATEGIC_MULTIPLIER
     - 1
     -
   * - SETTLER_BUILD_ON_COAST_PERCENT
     - 40
     -
   * - CITY_RING_1_MULTIPLIER
     - 6
     -
   * - CITY_RING_2_MULTIPLIER
     - 3
     -
   * - CITY_RING_3_MULTIPLIER
     - 2
     -
   * - CITY_RING_4_MULTIPLIER
     - 1
     -
   * - CITY_RING_5_MULTIPLIER
     - 1
     -
   * - SETTLER_EVALUATION_DISTANCE
     - 24
     -
   * - SETTLER_DISTANCE_DROPOFF_MODIFIER
     - 67
     -
   * - BUILD_ON_RESOURCE_PERCENT
     - -50
     -
   * - BUILD_ON_RIVER_PERCENT
     - 50
     -
   * - CHOKEPOINT_STRATEGIC_VALUE
     - 3
     -
   * - HILL_STRATEGIC_VALUE
     - 2
     -
   * - ALREADY_OWNED_STRATEGIC_VALUE
     - -1000
     -
   * - MINOR_CIV_CONTACT_GOLD_FIRST
     - 30
     -
   * - MINOR_CIV_CONTACT_GOLD_OTHER
     - 15
     -
   * - MINOR_CIV_GROWTH_PERCENT
     - 150
     -
   * - MINOR_CIV_PRODUCTION_PERCENT
     - 150
     -
   * - MINOR_CIV_GOLD_PERCENT
     - 200
     -
   * - MINOR_CIV_TECH_PERCENT
     - 40
     -
   * - MINOR_POLICY_RESOURCE_MULTIPLIER
     - 200
     -
   * - MINOR_POLICY_RESOURCE_HAPPINESS_MULTIPLIER
     - 150
     -
   * - MINOR_GOLD_GIFT_LARGE
     - 1000
     -
   * - MINOR_GOLD_GIFT_MEDIUM
     - 500
     -
   * - MINOR_GOLD_GIFT_SMALL
     - 250
     -
   * - MINOR_CIV_TILE_IMPROVEMENT_GIFT_COST
     - 200
     -
   * - MINOR_CIV_BUYOUT_COST
     - 500
     -
   * - MINOR_CIV_BUYOUT_TURNS
     - 5
     -
   * - MINOR_FRIENDSHIP_FROM_TRADE_MISSION
     - 30
     -
   * - MINOR_FRIENDSHIP_ANCHOR_DEFAULT
     - 0
     -
   * - MINOR_FRIENDSHIP_ANCHOR_MOD_PROTECTED
     - 5
     -
   * - MINOR_FRIENDSHIP_ANCHOR_MOD_WARY_OF
     - -20
     -
   * - MINOR_UNIT_GIFT_TRAVEL_TURNS
     - 3
     -
   * - PLOT_UNIT_LIMIT
     - 1
     -
   * - ZONE_OF_CONTROL_ENABLED
     - 1
     -
   * - FIRE_SUPPORT_DISABLED
     - 1
     -
   * - MAX_HIT_POINTS
     - 100
     -
   * - MAX_CITY_HIT_POINTS
     - 200
     -
   * - CITY_HIT_POINTS_HEALED_PER_TURN
     - 20
     -
   * - FLAT_LAND_EXTRA_DEFENSE
     - 0
     -
   * - HILLS_EXTRA_DEFENSE
     - 25
     -
   * - RIVER_ATTACK_MODIFIER
     - -20
     -
   * - AMPHIB_ATTACK_MODIFIER
     - -50
     -
   * - ENEMY_HEAL_RATE
     - 10
     - The amount of health a unit heals per-turn in enemy territory
   * - NEUTRAL_HEAL_RATE
     - 10
     - The amount of health a unit heals per-turn in neutral territory
   * - FRIENDLY_HEAL_RATE
     - 20
     - The amount of health a unit heals per-turn if inside an friendly city

       This includes friendly/allied city states, and other players where you have an open borders treaty.
   * - INSTA_HEAL_RATE
     - 50
     - The amount of health a unit gets if you choose to spend a promotion on instant-healing
   * - CITY_HEAL_RATE
     - 25
     - The amount of health a unit heals per-turn if inside the owning player's city
   * - ATTACK_SAME_STRENGTH_MIN_DAMAGE
     - 2400
     -
   * - RANGE_ATTACK_RANGED_DEFENDER_MOD
     - 100
     -
   * - ATTACK_SAME_STRENGTH_POSSIBLE_EXTRA_DAMAGE
     - 1200
     - Note: this will actually produce something between 0.00 and the listed number in damage
   * - RANGE_ATTACK_SAME_STRENGTH_MIN_DAMAGE
     - 2400
     -
   * - RANGE_ATTACK_SAME_STRENGTH_POSSIBLE_EXTRA_DAMAGE
     - 1200
     - Note: this will actually produce something between 0.00 and the listed number in damage
   * - AIR_STRIKE_SAME_STRENGTH_MIN_DEFENSE_DAMAGE
     - 2400
     -
   * - AIR_STRIKE_SAME_STRENGTH_POSSIBLE_EXTRA_DEFENSE_DAMAGE
     - 1200
     - Note: this will actually produce something between 0.00 and the listed number in damage
   * - INTERCEPTION_SAME_STRENGTH_MIN_DAMAGE
     - 2400
     -
   * - INTERCEPTION_SAME_STRENGTH_POSSIBLE_EXTRA_DAMAGE
     - 1200
     - Note: this will actually produce something between 0.00 and the listed number in damage
   * - AIR_SWEEP_INTERCEPTION_DAMAGE_MOD
     - 0
     -
   * - WOUNDED_DAMAGE_MULTIPLIER
     - 33
     -
   * - TRAIT_WOUNDED_DAMAGE_MOD
     - -33
     -
   * - CITY_STRENGTH_DEFAULT
     - 800
     -
   * - CITY_STRENGTH_POPULATION_CHANGE
     - 40
     -
   * - CITY_STRENGTH_UNIT_DIVISOR
     - 500
     -
   * - CITY_STRENGTH_TECH_BASE
     - 5.5
     -
   * - CITY_STRENGTH_TECH_EXPONENT
     - 2.8
     -
   * - CITY_STRENGTH_TECH_MULTIPLIER
     - 1
     -
   * - CITY_STRENGTH_HILL_CHANGE
     - 500
     -
   * - CITY_ATTACKING_DAMAGE_MOD
     - 100
     -
   * - ATTACKING_CITY_MELEE_DAMAGE_MOD
     - 100
     -
   * - CITY_ATTACK_RANGE
     - 2
     -
   * - CAN_CITY_USE_INDIRECT_FIRE
     - 1
     -
   * - CITY_RANGED_ATTACK_STRENGTH_MULTIPLIER
     - 75
     -
   * - MIN_CITY_STRIKE_DAMAGE
     - 10
     -
   * - CITY_CAPTURE_DAMAGE_PERCENT
     - 50
     -
   * - EXPERIENCE_PER_LEVEL
     - 10
     -
   * - EXPERIENCE_ATTACKING_UNIT_MELEE
     - 5
     -
   * - EXPERIENCE_DEFENDING_UNIT_MELEE
     - 4
     -
   * - EXPERIENCE_ATTACKING_UNIT_AIR
     - 4
     -
   * - EXPERIENCE_DEFENDING_UNIT_AIR
     - 2
     -
   * - EXPERIENCE_ATTACKING_UNIT_RANGED
     - 2
     -
   * - EXPERIENCE_DEFENDING_UNIT_RANGED
     - 2
     -
   * - EXPERIENCE_ATTACKING_AIR_SWEEP
     - 5
     -
   * - EXPERIENCE_DEFENDING_AIR_SWEEP_AIR
     - 5
     -
   * - EXPERIENCE_DEFENDING_AIR_SWEEP_GROUND
     - 2
     -
   * - EXPERIENCE_ATTACKING_CITY_MELEE
     - 5
     -
   * - EXPERIENCE_ATTACKING_CITY_RANGED
     - 3
     -
   * - EXPERIENCE_ATTACKING_CITY_AIR
     - 4
     -
   * - BARBARIAN_MAX_XP_VALUE
     - 30
     -
   * - COMBAT_EXPERIENCE_IN_BORDERS_PERCENT
     - 100
     -
   * - GREAT_GENERALS_THRESHOLD_INCREASE
     - 50
     -
   * - GREAT_GENERALS_THRESHOLD_INCREASE_TEAM
     - 50
     -
   * - GREAT_GENERALS_THRESHOLD
     - 200
     -
   * - UNIT_DEATH_XP_GREAT_GENERAL_LOSS
     - 50
     -
   * - MIN_EXPERIENCE_PER_COMBAT
     - 1
     - Deprecated (?) Experience & Promotion Define
   * - MAX_EXPERIENCE_PER_COMBAT
     - 10
     - Deprecated (?) Experience & Promotion Define
   * - CRAMPED_RANGE_FROM_CITY
     - 5
     -
   * - CRAMPED_USABLE_PLOT_PERCENT
     - 25
     -
   * - PROXIMITY_NEIGHBORS_CLOSEST_CITY_REQUIREMENT
     - 7
     -
   * - PROXIMITY_CLOSE_CLOSEST_CITY_POSSIBILITY
     - 11
     -
   * - PROXIMITY_CLOSE_DISTANCE_MAP_MULTIPLIER
     - 25
     -
   * - PROXIMITY_CLOSE_DISTANCE_MAX
     - 20
     -
   * - PROXIMITY_CLOSE_DISTANCE_MIN
     - 10
     -
   * - PROXIMITY_FAR_DISTANCE_MAP_MULTIPLIER
     - 45
     -
   * - PROXIMITY_FAR_DISTANCE_MAX
     - 50
     -
   * - PROXIMITY_FAR_DISTANCE_MIN
     - 20
     -
   * - PLOT_BASE_COST
     - 50
     - Related to cost of acquiring new Plots
   * - PLOT_ADDITIONAL_COST_PER_PLOT
     - 5
     - Related to cost of acquiring new Plots
   * - PLOT_COST_APPEARANCE_DIVISOR
     - 5
     - Related to cost of acquiring new Plots
   * - CULTURE_COST_FIRST_PLOT
     - 15
     - Related to cost of acquiring new Plots
   * - CULTURE_COST_LATER_PLOT_MULTIPLIER
     - 10
     - Related to cost of acquiring new Plots
   * - CULTURE_COST_LATER_PLOT_EXPONENT
     - 1.1
     - Related to cost of acquiring new Plots
   * - CULTURE_COST_VISIBLE_DIVISOR
     - 5
     - Related to cost of acquiring new Plots
   * - CULTURE_PLOT_COST_MOD_MINIMUM
     - -85
     - Related to cost of acquiring new Plots
   * - MINOR_CIV_PLOT_CULTURE_COST_MULTIPLIER
     - 150
     - Related to cost of acquiring new Plots
   * - MAXIMUM_BUY_PLOT_DISTANCE
     - 3
     -
   * - MAXIMUM_ACQUIRE_PLOT_DISTANCE
     - 5
     -
   * - PLOT_INFLUENCE_BASE_MULTIPLIER
     - 100
     -
   * - PLOT_INFLUENCE_DISTANCE_MULTIPLIER
     - 100
     -
   * - PLOT_INFLUENCE_DISTANCE_DIVISOR
     - 3
     -
   * - PLOT_INFLUENCE_RING_COST
     - 100
     -
   * - PLOT_INFLUENCE_WATER_COST
     - 25
     -
   * - PLOT_INFLUENCE_IMPROVEMENT_COST
     - -5
     -
   * - PLOT_INFLUENCE_ROUTE_COST
     - 0
     -
   * - PLOT_INFLUENCE_RESOURCE_COST
     - -105
     -
   * - PLOT_INFLUENCE_NW_COST
     - -105
     -
   * - PLOT_BUY_RESOURCE_COST
     - -100
     -
   * - PLOT_BUY_YIELD_COST
     - 10
     -
   * - PLOT_INFLUENCE_YIELD_POINT_COST
     - -1
     -
   * - PLOT_INFLUENCE_NO_ADJACENT_OWNED_COST
     - 1000
     -
   * - UNITED_NATIONS_COUNTDOWN_TURNS
     - 10
     - Old Diplomacy Victory
   * - OWN_UNITED_NATIONS_VOTE_BONUS
     - 1
     - Old Diplomacy Victory
   * - DIPLO_VICTORY_ALGORITHM_THRESHOLD
     - 28
     - Old Diplomacy Victory
   * - DIPLO_VICTORY_BEYOND_ALGORITHM_MULTIPLIER
     - 35
     - Old Diplomacy Victory
   * - DIPLO_VICTORY_TEAM_MULTIPLIER
     - 1.1
     - Old Diplomacy Victory
   * - DIPLO_VICTORY_DEFAULT_VOTE_PERCENT
     - 67
     - Old Diplomacy Victory
   * - DIPLO_VICTORY_CIV_DELEGATES_COEFFICIENT
     - 1.443
     - New Diplomacy Victory.  Desired values were calculated using logarithmic regression.  Number of Delegates needed to win = CivCoefficient * ln(numCivs) + CivConstant + CSCoefficient * ln(numCityStates) + CSConstant
   * - DIPLO_VICTORY_CIV_DELEGATES_CONSTANT
     - 7.000
     - New Diplomacy Victory.  Desired values were calculated using logarithmic regression.  Number of Delegates needed to win = CivCoefficient * ln(numCivs) + CivConstant + CSCoefficient * ln(numCityStates) + CSConstant
   * - DIPLO_VICTORY_CS_DELEGATES_COEFFICIENT
     - 16.023
     - New Diplomacy Victory.  Desired values were calculated using logarithmic regression.  Number of Delegates needed to win = CivCoefficient * ln(numCivs) + CivConstant + CSCoefficient * ln(numCityStates) + CSConstant
   * - DIPLO_VICTORY_CS_DELEGATES_CONSTANT
     - -13.758
     - New Diplomacy Victory.  Desired values were calculated using logarithmic regression.  Number of Delegates needed to win = CivCoefficient * ln(numCivs) + CivConstant + CSCoefficient * ln(numCityStates) + CSConstant
   * - SCORE_CITY_MULTIPLIER
     - 8
     - Scoring multiplier
   * - SCORE_POPULATION_MULTIPLIER
     - 4
     - Scoring multiplier
   * - SCORE_LAND_MULTIPLIER
     - 1
     - Scoring multiplier
   * - SCORE_WONDER_MULTIPLIER
     - 25
     - Scoring multiplier
   * - SCORE_TECH_MULTIPLIER
     - 4
     - Scoring multiplier
   * - SCORE_FUTURE_TECH_MULTIPLIER
     - 10
     - Scoring multiplier
   * - SCORE_POLICY_MULTIPLIER
     - 4
     - Scoring multiplier
   * - SCORE_GREAT_WORK_MULTIPLIER
     - 4
     - Scoring multiplier
   * - SCORE_BELIEF_MULTIPLIER
     - 20
     - Scoring multiplier
   * - SCORE_RELIGION_CITIES_MULTIPLIER
     - 1
     - Scoring multiplier
   * - RELIGION_MIN_FAITH_FIRST_PANTHEON
     - 10
     -
   * - RELIGION_MIN_FAITH_FIRST_PROPHET
     - 200
     -
   * - RELIGION_MIN_FAITH_FIRST_GREAT_PERSON
     - 1000
     -
   * - RELIGION_GAME_FAITH_DELTA_NEXT_PANTHEON
     - 5
     -
   * - RELIGION_FAITH_DELTA_NEXT_PROPHET
     - 100
     -
   * - RELIGION_FAITH_DELTA_NEXT_GREAT_PERSON
     - 500
     -
   * - RELIGION_BASE_CHANCE_PROPHET_SPAWN
     - 5
     -
   * - RELIGION_ATHEISM_PRESSURE_PER_POP
     - 1000
     -
   * - RELIGION_INITIAL_FOUNDING_CITY_PRESSURE
     - 5000
     -
   * - RELIGION_PER_TURN_FOUNDING_CITY_PRESSURE
     - 5
     -
   * - RELIGION_MISSIONARY_PRESSURE_MULTIPLIER
     - 10
     -
   * - RELIGION_ADJACENT_CITY_DISTANCE
     - 10
     -
   * - RELIGION_DIPLO_HIT_INITIAL_CONVERT_FRIENDLY_CITY
     - 1
     -
   * - RELIGION_DIPLO_HIT_RELIGIOUS_FLIP_FRIENDLY_CITY
     - 3
     -
   * - RELIGION_DIPLO_HIT_CONVERT_HOLY_CITY
     - 25
     -
   * - RELIGION_DIPLO_HIT_THRESHOLD
     - 5
     -
   * - ESPIONAGE_GATHERING_INTEL_COST_PERCENT
     - 125
     -
   * - ESPIONAGE_GATHERING_INTEL_RATE_BY_SPY_RANK_PERCENT
     - 25
     -
   * - ESPIONAGE_GATHERING_INTEL_RATE_BASE_PERCENT
     - 100
     -
   * - ESPIONAGE_TURNS_BETWEEN_CITY_STATE_ELECTIONS
     - 15
     -
   * - ESPIONAGE_INFLUENCE_GAINED_FOR_RIGGED_ELECTION
     - 20
     -
   * - ESPIONAGE_INFLUENCE_LOST_FOR_RIGGED_ELECTION
     - 5
     -
   * - ESPIONAGE_SURVEILLANCE_SIGHT_RANGE
     - 1
     -
   * - ESPIONAGE_COUP_OTHER_PLAYERS_INFLUENCE_DROP
     - 20
     -
   * - ESPIONAGE_COUP_NOBODY_BONUS
     - 0.5
     -
   * - ESPIONAGE_COUP_MULTIPLY_CONSTANT
     - 3.0
     -
   * - ESPIONAGE_COUP_SPY_LEVEL_DELTA_ZERO
     - 0.0
     -
   * - ESPIONAGE_COUP_SPY_LEVEL_DELTA_ONE
     - 1.5
     -
   * - ESPIONAGE_COUP_SPY_LEVEL_DELTA_TWO
     - 2.25
     -
   * - ESPIONAGE_COUP_SPY_LEVEL_DELTA_THREE
     - 2.60
     -
   * - ESPIONAGE_COUP_SPY_LEVEL_DELTA_FOUR
     - 2.80
     -
   * - INTERNATIONAL_TRADE_BASE
     - 100
     -
   * - INTERNATIONAL_TRADE_EXCLUSIVE_CONNECTION
     - 0
     -
   * - INTERNATIONAL_TRADE_CITY_GPT_DIVISOR
     - 20
     -
   * - LEAGUE_SESSION_INTERVAL_BASE_TURNS
     - 12
     - New Diplomacy Victory and Leagues
   * - LEAGUE_SESSION_SOON_WARNING_TURNS
     - 5
     - New Diplomacy Victory and Leagues
   * - LEAGUE_MEMBER_PROPOSALS_BASE
     - 1
     - New Diplomacy Victory and Leagues
   * - LEAGUE_MEMBER_VOTES_BASE
     - 1
     - New Diplomacy Victory and Leagues
   * - LEAGUE_MEMBER_VOTES_FOR_HOST
     - 1
     - New Diplomacy Victory and Leagues
   * - LEAGUE_MEMBER_VOTES_PER_CITY_STATE_ALLY
     - 1
     - New Diplomacy Victory and Leagues
   * - LEAGUE_PROJECT_REWARD_TIER_1_THRESHOLD
     - 0.5
     - New Diplomacy Victory and Leagues
   * - LEAGUE_PROJECT_REWARD_TIER_2_THRESHOLD
     - 1.0
     - New Diplomacy Victory and Leagues
   * - VICTORY_POINTS_PER_ERA
     - 1
     -
   * - PILLAGE_HEAL_AMOUNT
     - 25
     -
   * - CITY_CONNECTIONS_CONNECT_TO_CAPITAL
     - 1
     -
   * - MIN_GAME_TURNS_ELAPSED_TO_TEST_VICTORY
     - 10
     -
   * - ZERO_SUM_COMPETITION_WONDERS_VICTORY_POINTS
     - 5
     -
   * - ZERO_SUM_COMPETITION_POLICIES_VICTORY_POINTS
     - 5
     -
   * - ZERO_SUM_COMPETITION_GREAT_PEOPLE_VICTORY_POINTS
     - 5
     -
   * - MAX_CITY_DIST_HIGHWATER_MARK
     - 3
     -
   * - CITY_ZOOM_LEVEL_1
     - 500.0
     -
   * - CITY_ZOOM_LEVEL_2
     - 700.0
     -
   * - CITY_ZOOM_LEVEL_3
     - 800.0
     -
   * - CITY_ZOOM_OFFSET
     - 20.0
     -
   * - HEAVY_RESOURCE_THRESHOLD
     - 3
     -
   * - PROGRESS_POPUP_TURN_FREQUENCY
     - 25
     -
   * - SETTLER_PRODUCTION_SPEED
     - 0
     -
   * - BUY_PLOTS_DISABLED
     - 0
     -
   * - WARLORD_EXTRA_EXPERIENCE_PER_UNIT_PERCENT
     - 0
     -
   * - MINOR_CIV_ROUTE_QUEST_WEIGHT
     - 1000
     -
   * - WITHDRAW_MOD_ENEMY_MOVES
     - -20
     -
   * - WITHDRAW_MOD_BLOCKED_TILE
     - -20
     -
   * - ALLOW_EXTENDED_PLAY
     - 1
     -
   * - COMBAT_CAPTURE_HEALTH
     - 50
     -
   * - COMBAT_CAPTURE_MIN_CHANCE
     - 10
     -
   * - COMBAT_CAPTURE_MAX_CHANCE
     - 80
     -
   * - COMBAT_CAPTURE_RATIO_MULTIPLIER
     - 40
     -
   * - UNRESEARCHED_TECH_BONUS_FROM_KILLS_SLOPE
     - 9.68
     -
   * - UNRESEARCHED_TECH_BONUS_FROM_KILLS_INTERCEPT
     - -114.07
     -
   * - BASE_CULTURE_PER_GREAT_WORK
     - 2
     -
   * - BASE_TOURISM_PER_GREAT_WORK
     - 2
     -
   * - TOURISM_MODIFIER_SHARED_RELIGION
     - 25
     -
   * - TOURISM_MODIFIER_TRADE_ROUTE
     - 25
     -
   * - TOURISM_MODIFIER_OPEN_BORDERS
     - 25
     -
   * - TOURISM_MODIFIER_DIFFERENT_IDEOLOGIES
     - -34
     -
   * - TOURISM_MODIFIER_DIPLOMAT
     - 25
     -
   * - MINIUMUM_TOURISM_BLAST_STRENGTH
     - 100
     -
   * - CULTURE_LEVEL_EXOTIC
     - 10
     -
   * - CULTURE_LEVEL_FAMILIAR
     - 30
     -
   * - CULTURE_LEVEL_POPULAR
     - 60
     -
   * - CULTURE_LEVEL_INFLUENTIAL
     - 100
     -
   * - CULTURE_LEVEL_DOMINANT
     - 200
     -
   * - MIN_DIG_SITES_PER_MAJOR_CIV
     - 5
     -
   * - MAX_DIG_SITES_PER_MAJOR_CIV
     - 8
     -
   * - PERCENT_SITES_HIDDEN
     - 30
     -
   * - PERCENT_HIDDEN_SITES_WRITING
     - 30
     -
   * - SAPPED_CITY_ATTACK_MODIFIER
     - 50
     -
   * - SAPPER_BONUS_RANGE
     - 2
     -
   * - EXOTIC_GOODS_GOLD_MIN
     - 100
     -
   * - EXOTIC_GOODS_GOLD_MAX
     - 400
     -
   * - EXOTIC_GOODS_XP_MIN
     - 10
     -
   * - EXOTIC_GOODS_XP_MAX
     - 30
     -
   * - TEMPORARY_CULTURE_BOOST_MOD
     - 100
     -
   * - TEMPORARY_TOURISM_BOOST_MOD
     - 100
     -

TODO: Clean this up

.. code-block:: xml

		<Row Name="LAND_TERRAIN">
			<Key>TERRAIN_GRASS</Key>
			<Table>Terrains</Table>
		</Row>
		<Row Name="DEEP_WATER_TERRAIN">
			<Key>TERRAIN_OCEAN</Key>
			<Table>Terrains</Table>
		</Row>
		<Row Name="SHALLOW_WATER_TERRAIN">
			<Key>TERRAIN_COAST</Key>
			<Table>Terrains</Table>
		</Row>
		<Row Name="RUINS_IMPROVEMENT">
			<Key>IMPROVEMENT_CITY_RUINS</Key>
			<Table>Improvements</Table>
		</Row>
		<Row Name="NUKE_FEATURE">
			<Key>FEATURE_FALLOUT</Key>
			<Table>Features</Table>
		</Row>
		<Row Name="ARTIFACT_RESOURCE">
			<Key>RESOURCE_ARTIFACTS</Key>
			<Table>Resources</Table>
		</Row>
		<Row Name="HIDDEN_ARTIFACT_RESOURCE">
			<Key>RESOURCE_HIDDEN_ARTIFACTS</Key>
			<Table>Resources</Table>
		</Row>
		<Row Name="CAPITAL_BUILDINGCLASS">
			<Key>BUILDINGCLASS_PALACE</Key>
			<Table>BuildingClasses</Table>
		</Row>
		<Row Name="DEFAULT_SPECIALIST">
			<Key>SPECIALIST_CITIZEN</Key>
			<Table>Specialists</Table>
		</Row>
		<Row Name="SPACE_RACE_TRIGGER_PROJECT">
			<Key>PROJECT_APOLLO_PROGRAM</Key>
			<Table>Projects</Table>
		</Row>
		<Row Name="SPACESHIP_CAPSULE">
			<Key>PROJECT_SS_COCKPIT</Key>
			<Table>Projects</Table>
		</Row>
		<Row Name="SPACESHIP_BOOSTER">
			<Key>PROJECT_SS_BOOSTER</Key>
			<Table>Projects</Table>
		</Row>
		<Row Name="SPACESHIP_STASIS">
			<Key>PROJECT_SS_STASIS_CHAMBER</Key>
			<Table>Projects</Table>
		</Row>
		<Row Name="SPACESHIP_ENGINE">
			<Key>PROJECT_SS_ENGINE</Key>
			<Table>Projects</Table>
		</Row>
		<Row Name="MANHATTAN_PROJECT">
			<Key>PROJECT_MANHATTAN_PROJECT</Key>
			<Table>Projects</Table>
		</Row>
		<Row Name="INITIAL_CITY_ROUTE_TYPE">
			<Key>ROUTE_ROAD</Key>
			<Table>Routes</Table>
		</Row>
		<Row Name="STANDARD_HANDICAP">
			<Key>HANDICAP_CHIEFTAIN</Key>
			<Table>HandicapInfos</Table>
		</Row>
		<Row Name="MULTIPLAYER_HANDICAP">
			<Key>HANDICAP_PRINCE</Key>
			<Table>HandicapInfos</Table>
		</Row>
		<Row Name="STANDARD_HANDICAP_QUICK">
			<Key>HANDICAP_CHIEFTAIN</Key>
			<Table>HandicapInfos</Table>
		</Row>
		<Row Name="STANDARD_GAMESPEED">
			<Key>GAMESPEED_STANDARD</Key>
			<Table>GameSpeeds</Table>
		</Row>
		<Row Name="STANDARD_TURNTIMER">
			<Key>TURNTIMER_MEDIUM</Key>
			<Table>TurnTimers</Table>
		</Row>
		<Row Name="STANDARD_CLIMATE">
			<Key>CLIMATE_TEMPERATE</Key>
			<Table>Climates</Table>
		</Row>
		<Row Name="STANDARD_WORLD_SIZE">
			<Key>WORLDSIZE_STANDARD</Key>
			<Table>Worlds</Table>
		</Row>
		<Row Name="STANDARD_SEALEVEL">
			<Key>SEALEVEL_MEDIUM</Key>
			<Table>Sealevels</Table>
		</Row>
		<Row Name="STANDARD_ERA">
			<Key>ERA_ANCIENT</Key>
			<Table>Eras</Table>
		</Row>
		<Row Name="LAST_EMBARK_ART_ERA">
			<Key>ERA_INDUSTRIAL</Key>
			<Table>Eras</Table>
		</Row>
		<Row Name="LAST_UNIT_ART_ERA">
			<Key>ERA_INDUSTRIAL</Key>
			<Table>Eras</Table>
		</Row>
		<Row Name="LAST_BRIDGE_ART_ERA">
			<Key>ERA_MODERN</Key>
			<Table>Eras</Table>
		</Row>
		<Row Name="STANDARD_CALENDAR">
			<Key>CALENDAR_DEFAULT</Key>
			<Table>Calendars</Table>
		</Row>
		<Row Name="BARBARIAN_HANDICAP">
			<Key>HANDICAP_CHIEFTAIN</Key>
			<Table>HandicapInfos</Table>
		</Row>
		<Row Name="BARBARIAN_CIVILIZATION">
			<Key>CIVILIZATION_BARBARIAN</Key>
			<Table>Civilizations</Table>
		</Row>
		<Row Name="BARBARIAN_LEADER">
			<Key>LEADER_BARBARIAN</Key>
			<Table>Leaders</Table>
		</Row>
		<Row Name="MINOR_CIV_HANDICAP">
			<Key>HANDICAP_PRINCE</Key>
			<Table>HandicapInfos</Table>
		</Row>
		<Row Name="MINOR_CIVILIZATION">
			<Key>CIVILIZATION_MINOR</Key>
			<Table>Civilizations</Table>
		</Row>
		<Row Name="PROMOTION_EMBARKATION">
			<Key>PROMOTION_EMBARKATION</Key>
			<Table>UnitPromotions</Table>
		</Row>
		<Row Name="PROMOTION_DEFENSIVE_EMBARKATION">
			<Key>PROMOTION_DEFENSIVE_EMBARKATION</Key>
			<Table>UnitPromotions</Table>
		</Row>
		<Row Name="PROMOTION_ALLWATER_EMBARKATION">
			<Key>PROMOTION_ALLWATER_EMBARKATION</Key>
			<Table>UnitPromotions</Table>
		</Row>
		<Row Name="PROMOTION_OCEAN_IMPASSABLE">
			<Key>PROMOTION_OCEAN_IMPASSABLE</Key>
			<Table>UnitPromotions</Table>
		</Row>
		<Row Name="PROMOTION_OCEAN_IMPASSABLE_UNTIL_ASTRONOMY">
			<Key>PROMOTION_OCEAN_IMPASSABLE_UNTIL_ASTRONOMY</Key>
			<Table>UnitPromotions</Table>
		</Row>
		<Row Name="PROMOTION_ONLY_DEFENSIVE">
			<Key>PROMOTION_ONLY_DEFENSIVE</Key>
			<Table>UnitPromotions</Table>
		</Row>
		<Row Name="PROMOTION_UNWELCOME_EVANGELIST">
			<Key>PROMOTION_UNWELCOME_EVANGELIST</Key>
			<Table>UnitPromotions</Table>
		</Row>
		<Row Name="BARBARIAN_CAMP_IMPROVEMENT">
			<Key>IMPROVEMENT_BARBARIAN_CAMP</Key>
			<Table>Improvements</Table>
		</Row>
		<Row Name="WALLS_BUILDINGCLASS">
			<Key>BUILDINGCLASS_WALLS</Key>
			<Table>BuildingClasses</Table>
		</Row>
		<Row Name="PROMOTION_GOODY_HUT_PICKER">
			<Key>PROMOTION_GOODY_HUT_PICKER</Key>
			<Table>UnitPromotions</Table>
		</Row>
		<Row Name="POLICY_BRANCH_FREEDOM">
			<Key>POLICY_BRANCH_FREEDOM</Key>
			<Table>PolicyBranchTypes</Table>
		</Row>
		<Row Name="POLICY_BRANCH_AUTOCRACY">
			<Key>POLICY_BRANCH_AUTOCRACY</Key>
			<Table>PolicyBranchTypes</Table>
		</Row>
		<Row Name="POLICY_BRANCH_ORDER">
			<Key>POLICY_BRANCH_ORDER</Key>
			<Table>PolicyBranchTypes</Table>
		</Row>

Sources
-------

- ``Assets/Gameplay/XML/GlobalDefines.xml``
- ``Assets/DLC/Expansion/Gameplay/XML/GlobalDefines.xml``
- ``Assets/DLC/Expansion2/Gameplay/XML/GlobalDefines.xml``
