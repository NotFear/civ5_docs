Expansions
==========

.. contents:: :local:

.. _civ5_gak:

Gods & Kings
------------

Known internally as ``Expansion``, or ``Expansion1``

.. _civ5_bnw:

Brave New World
---------------

Known internally as ``Expansion2``
