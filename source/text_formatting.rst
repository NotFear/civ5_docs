Text formatting
===============

.. contents:: :local:

Special tags
------------

.. list-table::
   :header-rows: 1
   :widths: 1 100

   * - Tag
     - Notes

   * - ``[NEWLINE]``
     - Adds a line break
   * - ``[TAB]``
     - Adds a tab character
   * - ``[SPACE]``
     - Adds a space (useful at the beginning of a line in XML)
   * - ``[LINK]``
     - Adds a link - Example usage: ``[LINK=IMPROVEMENT_TRADING_POST]The trading post[/LINK]``
   * - ``[T]``
     - Unknown, usually seen used as ``[T]he future is not what it used to be.``

Colored text
------------

Text can be colored in two ways: either by prefixing it with a preset color tag, or with a custom color tag. When you
want text to no longer be colored, use the ``[ENDCOLOR]`` tag.

Preset color tags
^^^^^^^^^^^^^^^^^

Surround the name of a color with square brackets. A list of preset colors can be found in the :ref:`civ5_colors_table`
below.

Example: ``[COLOR_POSITIVE_TEXT]You did a good thing![ENDCOLOR]``

Custom color tags
^^^^^^^^^^^^^^^^^

Specify ``[COLOR:R:G:B:A]``, where R/G/B/A are replaced with red/green/blue/alpha values on a scale of 0 to 255.

Example: ``[COLOR:255:235:0:255]Here's some text![ENDCOLOR]``

.. _civ5_colors_table:

Colors table
^^^^^^^^^^^^

.. list-table::
   :header-rows: 1
   :widths: 20 80 1 1 1 1

   * - Example
     - Name
     - R
     - G
     - B
     - A

   * - :civ5_color_clear:`Example text`
     - ``COLOR_CLEAR``
     - 1.0
     - 1.0
     - 1.0
     - 0.0
   * - :civ5_color_alpha_grey:`Example text`
     - ``COLOR_ALPHA_GREY``
     - 0.1
     - 0.1
     - 0.1
     - 0.45
   * - :civ5_color_white:`Example text`
     - ``COLOR_WHITE``
     - 1.0
     - 1.0
     - 1.0
     - 1.0
   * - :civ5_color_black:`Example text`
     - ``COLOR_BLACK``
     - 0.0
     - 0.0
     - 0.0
     - 1.0
   * - :civ5_color_dark_grey:`Example text`
     - ``COLOR_DARK_GREY``
     - 0.25
     - 0.25
     - 0.25
     - 1.0
   * - :civ5_color_grey:`Example text`
     - ``COLOR_GREY``
     - 0.5
     - 0.5
     - 0.5
     - 1.0
   * - :civ5_color_light_grey:`Example text`
     - ``COLOR_LIGHT_GREY``
     - 0.75
     - 0.75
     - 0.75
     - 1.0
   * - :civ5_color_green:`Example text`
     - ``COLOR_GREEN``
     - 0.0
     - 1.0
     - 0.0
     - 1.0
   * - :civ5_color_blue:`Example text`
     - ``COLOR_BLUE``
     - 0.0
     - 0.0
     - 1.0
     - 1.0
   * - :civ5_color_xp_blue:`Example text`
     - ``COLOR_XP_BLUE``
     - 0.0
     - 0.47
     - 0.99
     - 1.0
   * - :civ5_color_cyan:`Example text`
     - ``COLOR_CYAN``
     - 0.0
     - 1.0
     - 1.0
     - 1.0
   * - :civ5_color_yellow:`Example text`
     - ``COLOR_YELLOW``
     - 1.0
     - 1.0
     - 0.0
     - 1.0
   * - :civ5_color_magenta:`Example text`
     - ``COLOR_MAGENTA``
     - 1.0
     - 0.0
     - 1.0
     - 1.0
   * - :civ5_color_yield_food:`Example text` |civ5_icon_food|
     - ``COLOR_YIELD_FOOD``
     - 0.99
     - 0.58
     - 0.16
     - 1.0
   * - :civ5_color_yield_production:`Example text` |civ5_icon_production|
     - ``COLOR_YIELD_PRODUCTION``
     - 0.44
     - 0.56
     - 0.74
     - 1.0
   * - :civ5_color_yield_gold:`Example text` |civ5_icon_gold|
     - ``COLOR_YIELD_GOLD``
     - 1.0
     - 0.94
     - 0.08
     - 1.0
   * - :civ5_color_city_blue:`Example text` |civ5_icon_capital|
     - ``COLOR_CITY_BLUE``
     - 0.07
     - 0.46
     - 0.8
     - 1.0
   * - :civ5_color_city_grey:`Example text` |civ5_icon_capital|
     - ``COLOR_CITY_GREY``
     - 0.35
     - 0.25
     - 0.25
     - 1.0
   * - :civ5_color_city_brown:`Example text` |civ5_icon_capital|
     - ``COLOR_CITY_BROWN``
     - 0.7
     - 0.45
     - 0.0
     - 1.0
   * - :civ5_color_city_green:`Example text` |civ5_icon_capital|
     - ``COLOR_CITY_GREEN``
     - 0.18
     - 0.53
     - 0.42
     - 1.0
   * - :civ5_color_font_red:`Example text`
     - ``COLOR_FONT_RED``
     - 1.0
     - 0.3
     - 0.15
     - 1.0
   * - :civ5_color_font_green:`Example text`
     - ``COLOR_FONT_GREEN``
     - 0.1
     - 0.95
     - 0.0
     - 1.0
   * - :civ5_color_research_stored:`Example text` |civ5_icon_research|
     - ``COLOR_RESEARCH_STORED``
     - 0.0
     - 1.0
     - 1.0
     - 1.0
   * - :civ5_color_research_rate:`Example text` |civ5_icon_research|
     - ``COLOR_RESEARCH_RATE``
     - 0.0
     - 1.0
     - 1.0
     - 0.6
   * - :civ5_color_culture_stored:`Example text` |civ5_icon_culture|
     - ``COLOR_CULTURE_STORED``
     - 0.6
     - 0.0
     - 1.0
     - 1.0
   * - :civ5_color_culture_rate:`Example text` |civ5_icon_culture|
     - ``COLOR_CULTURE_RATE``
     - 0.6
     - 0.0
     - 1.0
     - 0.6
   * - :civ5_color_great_people_stored:`Example text` |civ5_icon_great_people|
     - ``COLOR_GREAT_PEOPLE_STORED``
     - 1.0
     - 1.0
     - 0.0
     - 1.0
   * - :civ5_color_great_people_rate:`Example text` |civ5_icon_great_people|
     - ``COLOR_GREAT_PEOPLE_RATE``
     - 1.0
     - 1.0
     - 0.0
     - 0.6
   * - :civ5_color_negative_rate:`Example text`
     - ``COLOR_NEGATIVE_RATE``
     - 1.0
     - 0.0
     - 0.0
     - 0.65
   * - :civ5_color_empty:`Example text`
     - ``COLOR_EMPTY``
     - 0.0
     - 0.0
     - 0.0
     - 0.4
   * - :civ5_color_popup_text:`Example text`
     - ``COLOR_POPUP_TEXT``
     - 1.0
     - 1.0
     - 1.0
     - 1.0
   * - :civ5_color_popup_selected:`Example text`
     - ``COLOR_POPUP_SELECTED``
     - 1.0
     - 1.0
     - 0.0
     - 0.75
   * - :civ5_color_tech_text:`Example text` |civ5_icon_research|
     - ``COLOR_TECH_TEXT``
     - 0.5
     - 1.0
     - 0.1
     - 1.0
   * - :civ5_color_unit_text:`Example text`
     - ``COLOR_UNIT_TEXT``
     - 1.0
     - 1.0
     - 0.0
     - 1.0
   * - :civ5_color_building_text:`Example text`
     - ``COLOR_BUILDING_TEXT``
     - 0.8
     - 0.8
     - 0.85
     - 1.0
   * - :civ5_color_project_text:`Example text`
     - ``COLOR_PROJECT_TEXT``
     - 0.8
     - 0.8
     - 0.85
     - 1.0
   * - :civ5_color_highlight_text:`Example text`
     - ``COLOR_HIGHLIGHT_TEXT``
     - 0.4
     - 0.9
     - 1.0
     - 1.0
   * - :civ5_color_alt_highlight_text:`Example text`
     - ``COLOR_ALT_HIGHLIGHT_TEXT``
     - 0.5
     - 1.0
     - 0.1
     - 1.0
   * - :civ5_color_warning_text:`Example text`
     - ``COLOR_WARNING_TEXT``
     - 1.0
     - 0.3
     - 0.3
     - 1.0
   * - :civ5_color_positive_text:`Example text` |civ5_icon_happiness_1|
     - ``COLOR_POSITIVE_TEXT``
     - 0.5
     - 1.0
     - 0.1
     - 1.0
   * - :civ5_color_negative_text:`Example text` |civ5_icon_happiness_3|
     - ``COLOR_NEGATIVE_TEXT``
     - 1.0
     - 0.3
     - 0.3
     - 1.0
   * - :civ5_color_brown_text:`Example text`
     - ``COLOR_BROWN_TEXT``
     - 0.4
     - 0.24
     - 0.16
     - 1.0
   * - :civ5_color_selected_text:`Example text`
     - ``COLOR_SELECTED_TEXT``
     - 1.0
     - 0.82
     - 0.49
     - 1.0
   * - :civ5_color_water_text:`Example text`
     - ``COLOR_WATER_TEXT``
     - 0.7
     - 0.7
     - 1.0
     - 1.0
   * - :civ5_color_menu_blue:`Example text`
     - ``COLOR_MENU_BLUE``
     - 0.28
     - 0.83
     - 0.95
     - 1.0
   * - :civ5_color_dawn_of_man_text:`Example text`
     - ``COLOR_DAWN_OF_MAN_TEXT``
     - 0.22
     - 0.09
     - 0.03
     - 1.0
   * - :civ5_color_advisor_highlight_text:`Example text`
     - ``COLOR_ADVISOR_HIGHLIGHT_TEXT``
     - 1.0
     - 1.0
     - 0.0
     - 1.0
   * - :civ5_color_tech_green:`Example text` |civ5_icon_research|
     - ``COLOR_TECH_GREEN``
     - 0.16
     - 0.7
     - 0.27
     - 0.5
   * - :civ5_color_tech_blue:`Example text` |civ5_icon_research|
     - ``COLOR_TECH_BLUE``
     - 0.21
     - 0.23
     - 0.68
     - 0.5
   * - :civ5_color_tech_working:`Example text` |civ5_icon_research|
     - ``COLOR_TECH_WORKING``
     - 0.21
     - 0.23
     - 0.68
     - 0.5
   * - :civ5_color_tech_black:`Example text` |civ5_icon_research|
     - ``COLOR_TECH_BLACK``
     - 0.0
     - 0.0
     - 0.0
     - 0.5
   * - :civ5_color_tech_red:`Example text` |civ5_icon_research|
     - ``COLOR_TECH_RED``
     - 1.0
     - 0.0
     - 0.0
     - 0.5
   * - :civ5_color_red:`Example text`
     - ``COLOR_RED``
     - 1.0
     - 0.0
     - 0.0
     - 1.0
   * - :civ5_color_player_black:`Example text`
     - ``COLOR_PLAYER_BLACK``
     - 0.13
     - 0.13
     - 0.13
     - 1.0
   * - :civ5_color_player_black_text:`Example text`
     - ``COLOR_PLAYER_BLACK_TEXT``
     - 0.8
     - 0.808
     - 0.851
     - 1.0
   * - :civ5_color_player_blue:`Example text`
     - ``COLOR_PLAYER_BLUE``
     - 0.21
     - 0.4
     - 1.0
     - 1.0
   * - :civ5_color_player_light_blue_text:`Example text`
     - ``COLOR_PLAYER_LIGHT_BLUE_TEXT``
     - 0.7
     - 0.8
     - 1.0
     - 1.0
   * - :civ5_color_player_brown:`Example text`
     - ``COLOR_PLAYER_BROWN``
     - 0.39
     - 0.24
     - 0.0
     - 1.0
   * - :civ5_color_player_brown_text:`Example text`
     - ``COLOR_PLAYER_BROWN_TEXT``
     - 0.9
     - 0.65
     - 0.3
     - 1.0
   * - :civ5_color_player_cyan:`Example text`
     - ``COLOR_PLAYER_CYAN``
     - 0.07
     - 0.8
     - 0.96
     - 1.0
   * - :civ5_color_player_cyan_text:`Example text`
     - ``COLOR_PLAYER_CYAN_TEXT``
     - 0.6
     - 1.0
     - 0.973
     - 1.0
   * - :civ5_color_player_dark_blue:`Example text`
     - ``COLOR_PLAYER_DARK_BLUE``
     - 0.16
     - 0.0
     - 0.64
     - 1.0
   * - :civ5_color_player_dark_blue_text:`Example text`
     - ``COLOR_PLAYER_DARK_BLUE_TEXT``
     - 0.65
     - 0.55
     - 0.9
     - 1.0
   * - :civ5_color_player_dark_cyan:`Example text`
     - ``COLOR_PLAYER_DARK_CYAN``
     - 0.0
     - 0.54
     - 0.55
     - 1.0
   * - :civ5_color_player_dark_cyan_text:`Example text`
     - ``COLOR_PLAYER_DARK_CYAN_TEXT``
     - 0.0
     - 0.831
     - 0.788
     - 1.0
   * - :civ5_color_player_dark_green:`Example text`
     - ``COLOR_PLAYER_DARK_GREEN``
     - 0.0
     - 0.39
     - 0.0
     - 1.0
   * - :civ5_color_player_dark_dark_green:`Example text`
     - ``COLOR_PLAYER_DARK_DARK_GREEN``
     - 0.0
     - 0.27
     - 0.0
     - 1.0
   * - :civ5_color_player_dark_green_text:`Example text`
     - ``COLOR_PLAYER_DARK_GREEN_TEXT``
     - 0.561
     - 0.8
     - 0.561
     - 1.0
   * - :civ5_color_player_dark_pink:`Example text`
     - ``COLOR_PLAYER_DARK_PINK``
     - 0.69
     - 0.0
     - 0.38
     - 1.0
   * - :civ5_color_player_dark_pink_text:`Example text`
     - ``COLOR_PLAYER_DARK_PINK_TEXT``
     - 1.0
     - 0.0
     - 1.0
     - 1.0
   * - :civ5_color_player_dark_purple:`Example text`
     - ``COLOR_PLAYER_DARK_PURPLE``
     - 0.45
     - 0.0
     - 0.49
     - 1.0
   * - :civ5_color_player_dark_purple_text:`Example text`
     - ``COLOR_PLAYER_DARK_PURPLE_TEXT``
     - 0.8
     - 0.35
     - 0.85
     - 1.0
   * - :civ5_color_player_dark_red:`Example text`
     - ``COLOR_PLAYER_DARK_RED``
     - 0.62
     - 0.0
     - 0.0
     - 1.0
   * - :civ5_color_player_dark_red_text:`Example text`
     - ``COLOR_PLAYER_DARK_RED_TEXT``
     - 1.0
     - 0.22
     - 0.22
     - 1.0
   * - :civ5_color_player_dark_yellow:`Example text`
     - ``COLOR_PLAYER_DARK_YELLOW``
     - 0.97
     - 0.75
     - 0.0
     - 1.0
   * - :civ5_color_player_dark_yellow_text:`Example text`
     - ``COLOR_PLAYER_DARK_YELLOW_TEXT``
     - 1.0
     - 0.8
     - 0.0
     - 1.0
   * - :civ5_color_player_gray:`Example text`
     - ``COLOR_PLAYER_GRAY``
     - 0.7
     - 0.7
     - 0.7
     - 1.0
   * - :civ5_color_player_gray_text:`Example text`
     - ``COLOR_PLAYER_GRAY_TEXT``
     - 0.8
     - 0.8
     - 0.8
     - 1.0
   * - :civ5_color_player_green:`Example text`
     - ``COLOR_PLAYER_GREEN``
     - 0.49
     - 0.88
     - 0.0
     - 1.0
   * - :civ5_color_player_green_text:`Example text`
     - ``COLOR_PLAYER_GREEN_TEXT``
     - 0.486
     - 0.882
     - 0.0
     - 1.0
   * - :civ5_color_player_orange:`Example text`
     - ``COLOR_PLAYER_ORANGE``
     - 0.99
     - 0.35
     - 0.0
     - 1.0
   * - :civ5_color_player_orange_text:`Example text`
     - ``COLOR_PLAYER_ORANGE_TEXT``
     - 0.996
     - 0.459
     - 0.0
     - 1.0
   * - :civ5_color_player_peach:`Example text`
     - ``COLOR_PLAYER_PEACH``
     - 1.0
     - 0.85
     - 0.56
     - 1.0
   * - :civ5_color_player_peach_text:`Example text`
     - ``COLOR_PLAYER_PEACH_TEXT``
     - 0.761
     - 0.698
     - 0.396
     - 1.0
   * - :civ5_color_player_pink:`Example text`
     - ``COLOR_PLAYER_PINK``
     - 0.98
     - 0.67
     - 0.49
     - 1.0
   * - :civ5_color_player_pink_text:`Example text`
     - ``COLOR_PLAYER_PINK_TEXT``
     - 0.98
     - 0.72
     - 0.57
     - 1.0
   * - :civ5_color_player_purple:`Example text`
     - ``COLOR_PLAYER_PURPLE``
     - 0.77
     - 0.34
     - 1.0
     - 1.0
   * - :civ5_color_player_purple_text:`Example text`
     - ``COLOR_PLAYER_PURPLE_TEXT``
     - 0.85
     - 0.65
     - 1.0
     - 1.0
   * - :civ5_color_player_red:`Example text`
     - ``COLOR_PLAYER_RED``
     - 0.86
     - 0.02
     - 0.02
     - 1.0
   * - :civ5_color_player_red_text:`Example text`
     - ``COLOR_PLAYER_RED_TEXT``
     - 1.0
     - 0.298
     - 0.416
     - 1.0
   * - :civ5_color_player_white:`Example text`
     - ``COLOR_PLAYER_WHITE``
     - 0.9
     - 0.9
     - 0.9
     - 1.0
   * - :civ5_color_player_white_text:`Example text`
     - ``COLOR_PLAYER_WHITE_TEXT``
     - 1.0
     - 0.95
     - 0.95
     - 1.0
   * - :civ5_color_player_yellow:`Example text`
     - ``COLOR_PLAYER_YELLOW``
     - 1.0
     - 1.0
     - 0.17
     - 1.0
   * - :civ5_color_player_yellow_text:`Example text`
     - ``COLOR_PLAYER_YELLOW_TEXT``
     - 0.996
     - 1.0
     - 0.173
     - 1.0
   * - :civ5_color_player_light_green:`Example text`
     - ``COLOR_PLAYER_LIGHT_GREEN``
     - 0.5
     - 1.0
     - 0.5
     - 1.0
   * - :civ5_color_player_light_green_text:`Example text`
     - ``COLOR_PLAYER_LIGHT_GREEN_TEXT``
     - 0.7
     - 1.0
     - 0.7
     - 1.0
   * - :civ5_color_player_light_blue:`Example text`
     - ``COLOR_PLAYER_LIGHT_BLUE``
     - 0.5
     - 0.7
     - 1.0
     - 1.0
   * - :civ5_color_player_blue_text:`Example text`
     - ``COLOR_PLAYER_BLUE_TEXT``
     - 0.5
     - 0.7
     - 1.0
     - 1.0
   * - :civ5_color_player_light_yellow:`Example text`
     - ``COLOR_PLAYER_LIGHT_YELLOW``
     - 1.0
     - 1.0
     - 0.5
     - 1.0
   * - :civ5_color_player_light_yellow_text:`Example text`
     - ``COLOR_PLAYER_LIGHT_YELLOW_TEXT``
     - 0.9
     - 1.0
     - 0.5
     - 1.0
   * - :civ5_color_player_light_purple:`Example text`
     - ``COLOR_PLAYER_LIGHT_PURPLE``
     - 0.7
     - 0.6
     - 1.0
     - 1.0
   * - :civ5_color_player_light_purple_text:`Example text`
     - ``COLOR_PLAYER_LIGHT_PURPLE_TEXT``
     - 0.7
     - 0.6
     - 1.0
     - 1.0
   * - :civ5_color_player_light_orange:`Example text`
     - ``COLOR_PLAYER_LIGHT_ORANGE``
     - 0.9
     - 0.65
     - 0.32
     - 1.0
   * - :civ5_color_player_light_orange_text:`Example text`
     - ``COLOR_PLAYER_LIGHT_ORANGE_TEXT``
     - 1.0
     - 0.75
     - 0.35
     - 1.0
   * - :civ5_color_player_middle_purple:`Example text`
     - ``COLOR_PLAYER_MIDDLE_PURPLE``
     - 0.675
     - 0.118
     - 0.725
     - 1.0
   * - :civ5_color_player_middle_purple_text:`Example text`
     - ``COLOR_PLAYER_MIDDLE_PURPLE_TEXT``
     - 0.808
     - 0.396
     - 0.631
     - 1.0
   * - :civ5_color_player_goldenrod:`Example text`
     - ``COLOR_PLAYER_GOLDENROD``
     - 0.871
     - 0.624
     - 0.0
     - 1.0
   * - :civ5_color_player_dark_gray:`Example text`
     - ``COLOR_PLAYER_DARK_GRAY``
     - 0.369
     - 0.369
     - 0.369
     - 1.0
   * - :civ5_color_player_dark_gray_text:`Example text`
     - ``COLOR_PLAYER_DARK_GRAY_TEXT``
     - 0.565
     - 0.565
     - 0.565
     - 1.0
   * - :civ5_color_player_middle_green:`Example text`
     - ``COLOR_PLAYER_MIDDLE_GREEN``
     - 0.204
     - 0.576
     - 0.0
     - 1.0
   * - :civ5_color_player_middle_green_text:`Example text`
     - ``COLOR_PLAYER_MIDDLE_GREEN_TEXT``
     - 0.243
     - 0.616
     - 0.0
     - 1.0
   * - :civ5_color_player_dark_lemon:`Example text`
     - ``COLOR_PLAYER_DARK_LEMON``
     - 0.847
     - 0.792
     - 0.039
     - 1.0
   * - :civ5_color_player_dark_lemon_text:`Example text`
     - ``COLOR_PLAYER_DARK_LEMON_TEXT``
     - 0.906
     - 0.851
     - 0.098
     - 1.0
   * - :civ5_color_player_middle_blue:`Example text`
     - ``COLOR_PLAYER_MIDDLE_BLUE``
     - 0.0
     - 0.22
     - 0.914
     - 1.0
   * - :civ5_color_player_middle_blue_text:`Example text`
     - ``COLOR_PLAYER_MIDDLE_BLUE_TEXT``
     - 0.504
     - 0.725
     - 0.71
     - 1.0
   * - :civ5_color_player_middle_cyan:`Example text`
     - ``COLOR_PLAYER_MIDDLE_CYAN``
     - 0.0
     - 0.639
     - 0.71
     - 1.0
   * - :civ5_color_player_middle_cyan_text:`Example text`
     - ``COLOR_PLAYER_MIDDLE_CYAN_TEXT``
     - 0.157
     - 0.796
     - 0.867
     - 1.0
   * - :civ5_color_player_maroon:`Example text`
     - ``COLOR_PLAYER_MAROON``
     - 0.514
     - 0.2
     - 0.157
     - 1.0
   * - :civ5_color_player_light_brown:`Example text`
     - ``COLOR_PLAYER_LIGHT_BROWN``
     - 0.518
     - 0.345
     - 0.075
     - 1.0
   * - :civ5_color_player_light_brown_text:`Example text`
     - ``COLOR_PLAYER_LIGHT_BROWN_TEXT``
     - 0.58
     - 0.431
     - 0.204
     - 1.0
   * - :civ5_color_player_dark_orange:`Example text`
     - ``COLOR_PLAYER_DARK_ORANGE``
     - 0.878
     - 0.235
     - 0.0
     - 1.0
   * - :civ5_color_player_dark_orange_text:`Example text`
     - ``COLOR_PLAYER_DARK_ORANGE_TEXT``
     - 0.949
     - 0.306
     - 0.071
     - 1.0
   * - :civ5_color_player_dark_dark_green_text:`Example text`
     - ``COLOR_PLAYER_DARK_DARK_GREEN_TEXT``
     - 0.355
     - 0.625
     - 0.355
     - 1.0
   * - :civ5_color_player_pale_red:`Example text`
     - ``COLOR_PLAYER_PALE_RED``
     - 0.78
     - 0.282
     - 0.239
     - 1.0
   * - :civ5_color_player_dark_indigo:`Example text`
     - ``COLOR_PLAYER_DARK_INDIGO``
     - 0.306
     - 0.02
     - 0.835
     - 1.0
   * - :civ5_color_player_dark_indigo_text:`Example text`
     - ``COLOR_PLAYER_DARK_INDIGO_TEXT``
     - 0.533
     - 0.361
     - 0.859
     - 1.0
   * - :civ5_color_player_pale_orange:`Example text`
     - ``COLOR_PLAYER_PALE_ORANGE``
     - 0.863
     - 0.471
     - 0.149
     - 1.0
   * - :civ5_color_player_light_black:`Example text`
     - ``COLOR_PLAYER_LIGHT_BLACK``
     - 0.251
     - 0.251
     - 0.251
     - 1.0
   * - :civ5_color_player_light_black_text:`Example text`
     - ``COLOR_PLAYER_LIGHT_BLACK_TEXT``
     - 0.392
     - 0.392
     - 0.392
     - 1.0
   * - :civ5_color_player_minor_icon:`Example text`
     - ``COLOR_PLAYER_MINOR_ICON``
     - 0.0
     - 0.0
     - 0.0
     - 1.0
   * - :civ5_color_player_barbarian_icon:`Example text`
     - ``COLOR_PLAYER_BARBARIAN_ICON``
     - 0.721
     - 0.0
     - 0.0
     - 1.0
   * - :civ5_color_player_america_icon:`Example text`
     - ``COLOR_PLAYER_AMERICA_ICON``
     - 1.0
     - 1.0
     - 1.0
     - 1.0
   * - :civ5_color_player_arabia_icon:`Example text`
     - ``COLOR_PLAYER_ARABIA_ICON``
     - 0.573
     - 0.867
     - 0.039
     - 1.0
   * - :civ5_color_player_aztec_icon:`Example text`
     - ``COLOR_PLAYER_AZTEC_ICON``
     - 0.537
     - 0.937
     - 0.835
     - 1.0
   * - :civ5_color_player_china_icon:`Example text`
     - ``COLOR_PLAYER_CHINA_ICON``
     - 1.0
     - 1.0
     - 1.0
     - 1.0
   * - :civ5_color_player_egypt_icon:`Example text`
     - ``COLOR_PLAYER_EGYPT_ICON``
     - 0.325
     - 0.0
     - 0.816
     - 1.0
   * - :civ5_color_player_england_icon:`Example text`
     - ``COLOR_PLAYER_ENGLAND_ICON``
     - 1.0
     - 1.0
     - 1.0
     - 1.0
   * - :civ5_color_player_france_icon:`Example text`
     - ``COLOR_PLAYER_FRANCE_ICON``
     - 0.922
     - 0.922
     - 0.545
     - 1.0
   * - :civ5_color_player_germany_icon:`Example text`
     - ``COLOR_PLAYER_GERMANY_ICON``
     - 0.145
     - 0.169
     - 0.129
     - 1.0
   * - :civ5_color_player_greece_icon:`Example text`
     - ``COLOR_PLAYER_GREECE_ICON``
     - 0.255
     - 0.553
     - 0.996
     - 1.0
   * - :civ5_color_player_india_icon:`Example text`
     - ``COLOR_PLAYER_INDIA_ICON``
     - 1.0
     - 0.6
     - 0.196
     - 1.0
   * - :civ5_color_player_iroquois_icon:`Example text`
     - ``COLOR_PLAYER_IROQUOIS_ICON``
     - 0.988
     - 0.792
     - 0.506
     - 1.0
   * - :civ5_color_player_japan_icon:`Example text`
     - ``COLOR_PLAYER_JAPAN_ICON``
     - 0.722
     - 0.0
     - 0.0
     - 1.0
   * - :civ5_color_player_ottoman_icon:`Example text`
     - ``COLOR_PLAYER_OTTOMAN_ICON``
     - 0.071
     - 0.322
     - 0.118
     - 1.0
   * - :civ5_color_player_persia_icon:`Example text`
     - ``COLOR_PLAYER_PERSIA_ICON``
     - 0.961
     - 0.902
     - 0.216
     - 1.0
   * - :civ5_color_player_rome_icon:`Example text`
     - ``COLOR_PLAYER_ROME_ICON``
     - 0.941
     - 0.78
     - 0.0
     - 1.0
   * - :civ5_color_player_russia_icon:`Example text`
     - ``COLOR_PLAYER_RUSSIA_ICON``
     - 0.0
     - 0.0
     - 0.0
     - 1.0
   * - :civ5_color_player_siam_icon:`Example text`
     - ``COLOR_PLAYER_SIAM_ICON``
     - 0.694
     - 0.031
     - 0.012
     - 1.0
   * - :civ5_color_player_songhai_icon:`Example text`
     - ``COLOR_PLAYER_SONGHAI_ICON``
     - 0.353
     - 0.0
     - 0.039
     - 1.0
   * - :civ5_color_player_barbarian_background:`Example text`
     - ``COLOR_PLAYER_BARBARIAN_BACKGROUND``
     - 0.0
     - 0.0
     - 0.0
     - 1.0
   * - :civ5_color_player_america_background:`Example text`
     - ``COLOR_PLAYER_AMERICA_BACKGROUND``
     - 0.122
     - 0.2
     - 0.471
     - 1.0
   * - :civ5_color_player_arabia_background:`Example text`
     - ``COLOR_PLAYER_ARABIA_BACKGROUND``
     - 0.169
     - 0.345
     - 0.18
     - 1.0
   * - :civ5_color_player_aztec_background:`Example text`
     - ``COLOR_PLAYER_AZTEC_BACKGROUND``
     - 0.633
     - 0.224
     - 0.137
     - 1.0
   * - :civ5_color_player_china_background:`Example text`
     - ``COLOR_PLAYER_CHINA_BACKGROUND``
     - 0.0
     - 0.584
     - 0.322
     - 1.0
   * - :civ5_color_player_egypt_background:`Example text`
     - ``COLOR_PLAYER_EGYPT_BACKGROUND``
     - 1.0
     - 0.988
     - 0.012
     - 1.0
   * - :civ5_color_player_england_background:`Example text`
     - ``COLOR_PLAYER_ENGLAND_BACKGROUND``
     - 0.427
     - 0.008
     - 0.0
     - 1.0
   * - :civ5_color_player_france_background:`Example text`
     - ``COLOR_PLAYER_FRANCE_BACKGROUND``
     - 0.255
     - 0.553
     - 0.996
     - 1.0
   * - :civ5_color_player_germany_background:`Example text`
     - ``COLOR_PLAYER_GERMANY_BACKGROUND``
     - 0.702
     - 0.698
     - 0.722
     - 1.0
   * - :civ5_color_player_greece_background:`Example text`
     - ``COLOR_PLAYER_GREECE_BACKGROUND``
     - 1.0
     - 1.0
     - 1.0
     - 1.0
   * - :civ5_color_player_india_background:`Example text`
     - ``COLOR_PLAYER_INDIA_BACKGROUND``
     - 0.071
     - 0.533
     - 0.027
     - 1.0
   * - :civ5_color_player_iroquois_background:`Example text`
     - ``COLOR_PLAYER_IROQUOIS_BACKGROUND``
     - 0.255
     - 0.341
     - 0.341
     - 1.0
   * - :civ5_color_player_japan_background:`Example text`
     - ``COLOR_PLAYER_JAPAN_BACKGROUND``
     - 1.0
     - 1.0
     - 1.0
     - 1.0
   * - :civ5_color_player_ottoman_background:`Example text`
     - ``COLOR_PLAYER_OTTOMAN_BACKGROUND``
     - 0.969
     - 0.976
     - 0.784
     - 1.0
   * - :civ5_color_player_persia_background:`Example text`
     - ``COLOR_PLAYER_PERSIA_BACKGROUND``
     - 0.694
     - 0.031
     - 0.012
     - 1.0
   * - :civ5_color_player_rome_background:`Example text`
     - ``COLOR_PLAYER_ROME_BACKGROUND``
     - 0.275
     - 0.0
     - 0.463
     - 1.0
   * - :civ5_color_player_russia_background:`Example text`
     - ``COLOR_PLAYER_RUSSIA_BACKGROUND``
     - 0.937
     - 0.706
     - 0.0
     - 1.0
   * - :civ5_color_player_siam_background:`Example text`
     - ``COLOR_PLAYER_SIAM_BACKGROUND``
     - 0.961
     - 0.902
     - 0.216
     - 1.0
   * - :civ5_color_player_songhai_background:`Example text`
     - ``COLOR_PLAYER_SONGHAI_BACKGROUND``
     - 0.839
     - 0.569
     - 0.075
     - 1.0

Sources
^^^^^^^

- Colors are defined in the database table ``Colors``, which is populated from
  ``Assets/Gameplay/XML/Interface/CIV5Colors.xml``

Font icons
----------

Icons can be inserted inline in text in most places by taking the name of an icon and surrounding it in square brackets.
For example, you can name a religion ``[ICON_RES_SALT]`` and it will show up as |civ5_icon_res_salt|.

.. list-table::
   :header-rows: 1
   :widths: 1 20 80 1

   * - Icon
     - Name
     - Notes
     - Expansion

   * - |civ5_icon_bullet|
     - ``ICON_BULLET``
     -
     -
   * - |civ5_icon_research|
     - ``ICON_RESEARCH``
     -
     -
   * - |civ5_icon_gold|
     - ``ICON_GOLD``
     -
     -
   * - |civ5_icon_production|
     - ``ICON_PRODUCTION``
     -
     -
   * - |civ5_icon_food|
     - ``ICON_FOOD``
     -
     -
   * - |civ5_icon_culture|
     - ``ICON_CULTURE``
     -
     -
   * - |civ5_icon_moves|
     - ``ICON_MOVES``
     -
     -
   * - |civ5_icon_range_strength|
     - ``ICON_RANGE_STRENGTH``
     -
     -
   * - |civ5_icon_strength|
     - ``ICON_STRENGTH``
     -
     -
   * - |civ5_icon_res_iron|
     - ``ICON_RES_IRON``
     -
     -
   * - |civ5_icon_res_horse|
     - ``ICON_RES_HORSE``
     -
     -
   * - |civ5_icon_res_coal|
     - ``ICON_RES_COAL``
     -
     -
   * - |civ5_icon_res_oil|
     - ``ICON_RES_OIL``
     -
     -
   * - |civ5_icon_res_aluminum|
     - ``ICON_RES_ALUMINUM``
     -
     -
   * - |civ5_icon_res_uranium|
     - ``ICON_RES_URANIUM``
     -
     -
   * - |civ5_icon_res_wheat|
     - ``ICON_RES_WHEAT``
     -
     -
   * - |civ5_icon_res_cow|
     - ``ICON_RES_COW``
     -
     -
   * - |civ5_icon_res_deer|
     - ``ICON_RES_DEER``
     -
     -
   * - |civ5_icon_res_fish|
     - ``ICON_RES_FISH``
     -
     -
   * - |civ5_icon_res_whale|
     - ``ICON_RES_WHALE``
     -
     -
   * - |civ5_icon_res_banana|
     - ``ICON_RES_BANANA``
     -
     -
   * - |civ5_icon_res_gold|
     - ``ICON_RES_GOLD``
     -
     -
   * - |civ5_icon_res_gems|
     - ``ICON_RES_GEMS``
     -
     -
   * - |civ5_icon_res_marble|
     - ``ICON_RES_MARBLE``
     -
     -
   * - |civ5_icon_res_ivory|
     - ``ICON_RES_IVORY``
     -
     -
   * - |civ5_icon_res_dye|
     - ``ICON_RES_DYE``
     -
     -
   * - |civ5_icon_res_spices|
     - ``ICON_RES_SPICES``
     -
     -
   * - |civ5_icon_res_silk|
     - ``ICON_RES_SILK``
     -
     -
   * - |civ5_icon_res_sugar|
     - ``ICON_RES_SUGAR``
     -
     -
   * - |civ5_icon_res_cotton|
     - ``ICON_RES_COTTON``
     -
     -
   * - |civ5_icon_happiness_1|
     - ``ICON_HAPPINESS_1``
     -
     -
   * - |civ5_icon_happiness_2|
     - ``ICON_HAPPINESS_2``
     -
     -
   * - |civ5_icon_happiness_3|
     - ``ICON_HAPPINESS_3``
     -
     -
   * - |civ5_icon_happiness_4|
     - ``ICON_HAPPINESS_4``
     -
     -
   * - |civ5_icon_golden_age|
     - ``ICON_GOLDEN_AGE``
     -
     -
   * - |civ5_icon_plus|
     - ``ICON_PLUS``
     -
     -
   * - |civ5_icon_minus|
     - ``ICON_MINUS``
     -
     -
   * - |civ5_icon_wtf1|
     - ``ICON_WTF1``
     -
     -
   * - |civ5_icon_wtf2|
     - ``ICON_WTF2``
     -
     -
   * - |civ5_icon_res_pearls|
     - ``ICON_RES_PEARLS``
     -
     -
   * - |civ5_icon_res_incense|
     - ``ICON_RES_INCENSE``
     -
     -
   * - |civ5_icon_res_wine|
     - ``ICON_RES_WINE``
     -
     -
   * - |civ5_icon_res_silver|
     - ``ICON_RES_SILVER``
     -
     -
   * - |civ5_icon_res_fur|
     - ``ICON_RES_FUR``
     -
     -
   * - |civ5_icon_citizen|
     - ``ICON_CITIZEN``
     -
     -
   * - |civ5_icon_great_people|
     - ``ICON_GREAT_PEOPLE``
     -
     -
   * - |civ5_icon_razing|
     - ``ICON_RAZING``
     -
     -
   * - |civ5_icon_resistance|
     - ``ICON_RESISTANCE``
     -
     -
   * - |civ5_icon_connected|
     - ``ICON_CONNECTED``
     -
     -
   * - |civ5_icon_blockaded|
     - ``ICON_BLOCKADED``
     -
     -
   * - |civ5_icon_puppet|
     - ``ICON_PUPPET``
     -
     -
   * - |civ5_icon_occupied|
     - ``ICON_OCCUPIED``
     -
     -
   * - |civ5_icon_influence|
     - ``ICON_INFLUENCE``
     -
     -
   * - |civ5_icon_capital|
     - ``ICON_CAPITAL``
     -
     -
   * - |civ5_icon_locked|
     - ``ICON_LOCKED``
     -
     -
   * - |civ5_icon_res_sheep|
     - ``ICON_RES_SHEEP``
     -
     -
   * - |civ5_icon_war|
     - ``ICON_WAR``
     -
     -
   * - |civ5_icon_peace|
     - ``ICON_PEACE``
     -
     -
   * - |civ5_icon_trade|
     - ``ICON_TRADE``
     -
     -
   * - |civ5_icon_trade_white|
     - ``ICON_TRADE_WHITE``
     -
     -
   * - |civ5_icon_team_1|
     - ``ICON_TEAM_1``
     -
     -
   * - |civ5_icon_team_2|
     - ``ICON_TEAM_2``
     -
     -
   * - |civ5_icon_team_3|
     - ``ICON_TEAM_3``
     -
     -
   * - |civ5_icon_team_4|
     - ``ICON_TEAM_4``
     -
     -
   * - |civ5_icon_team_5|
     - ``ICON_TEAM_5``
     -
     -
   * - |civ5_icon_team_6|
     - ``ICON_TEAM_6``
     -
     -
   * - |civ5_icon_team_7|
     - ``ICON_TEAM_7``
     -
     -
   * - |civ5_icon_team_8|
     - ``ICON_TEAM_8``
     -
     -
   * - |civ5_icon_team_9|
     - ``ICON_TEAM_9``
     -
     -
   * - |civ5_icon_team_10|
     - ``ICON_TEAM_10``
     -
     -
   * - |civ5_icon_team_11|
     - ``ICON_TEAM_11``
     -
     -
   * - |civ5_icon_team_usa|
     - ``ICON_TEAM_USA``
     -
     -
   * - |civ5_icon_pirate|
     - ``ICON_PIRATE``
     -
     -
   * - |civ5_icon_star|
     - ``ICON_STAR``
     -
     -
   * - |civ5_icon_mushroom|
     - ``ICON_MUSHROOM``
     -
     -
   * - |civ5_icon_flower|
     - ``ICON_FLOWER``
     -
     -
   * - |civ5_icon_alpha|
     - ``ICON_ALPHA``
     -
     -
   * - |civ5_icon_omega|
     - ``ICON_OMEGA``
     -
     -
   * - |civ5_icon_city_state|
     - ``ICON_CITY_STATE``
     -
     -
   * - |civ5_icon_res_stone|
     - ``ICON_RES_STONE``
     -
     -
   * - |civ5_icon_swap|
     - ``ICON_SWAP``
     -
     -

   * - |civ5_icon_res_citrus|
     - ``ICON_RES_CITRUS``
     -
     - |civ5_gak|
   * - |civ5_icon_res_copper|
     - ``ICON_RES_COPPER``
     -
     - |civ5_gak|
   * - |civ5_icon_res_crab|
     - ``ICON_RES_CRAB``
     -
     - |civ5_gak|
   * - |civ5_icon_res_salt|
     - ``ICON_RES_SALT``
     -
     - |civ5_gak|
   * - |civ5_icon_res_truffles|
     - ``ICON_RES_TRUFFLES``
     -
     - |civ5_gak|
   * - |civ5_icon_res_jewelry|
     - ``ICON_RES_JEWELRY``
     -
     - |civ5_gak|
   * - |civ5_icon_res_porcelain|
     - ``ICON_RES_PORCELAIN``
     -
     - |civ5_gak|
   * - |civ5_icon_religion_buddhism|
     - ``ICON_RELIGION_BUDDHISM``
     -
     - |civ5_gak|
   * - |civ5_icon_religion_christianity|
     - ``ICON_RELIGION_CHRISTIANITY``
     -
     - |civ5_gak|
   * - |civ5_icon_religion_confucianism|
     - ``ICON_RELIGION_CONFUCIANISM``
     -
     - |civ5_gak|
   * - |civ5_icon_religion_hinduism|
     - ``ICON_RELIGION_HINDUISM``
     -
     - |civ5_gak|
   * - |civ5_icon_religion_islam|
     - ``ICON_RELIGION_ISLAM``
     -
     - |civ5_gak|
   * - |civ5_icon_religion_judaism|
     - ``ICON_RELIGION_JUDAISM``
     -
     - |civ5_gak|
   * - |civ5_icon_religion_pantheon|
     - ``ICON_RELIGION_PANTHEON``
     -
     - |civ5_gak|
   * - |civ5_icon_religion_shinto|
     - ``ICON_RELIGION_SHINTO``
     -
     - |civ5_gak|
   * - |civ5_icon_religion_sikhism|
     - ``ICON_RELIGION_SIKHISM``
     -
     - |civ5_gak|
   * - |civ5_icon_religion_taoism|
     - ``ICON_RELIGION_TAOISM``
     -
     - |civ5_gak|
   * - |civ5_icon_religion_tengriism|
     - ``ICON_RELIGION_TENGRIISM``
     -
     - |civ5_gak|
   * - |civ5_icon_religion_zoroastrianism|
     - ``ICON_RELIGION_ZOROASTRIANISM``
     -
     - |civ5_gak|
   * - |civ5_icon_religion_orthodox|
     - ``ICON_RELIGION_ORTHODOX``
     -
     - |civ5_gak|
   * - |civ5_icon_religion_protestant|
     - ``ICON_RELIGION_PROTESTANT``
     -
     - |civ5_gak|
   * - |civ5_icon_religion|
     - ``ICON_RELIGION``
     -
     - |civ5_gak|
   * - |civ5_icon_missionary|
     - ``ICON_MISSIONARY``
     -
     - |civ5_gak|
   * - |civ5_icon_prophet|
     - ``ICON_PROPHET``
     -
     - |civ5_gak|
   * - |civ5_icon_inquisitor|
     - ``ICON_INQUISITOR``
     -
     - |civ5_gak|
   * - |civ5_icon_spy|
     - ``ICON_SPY``
     -
     - |civ5_gak|
   * - |civ5_icon_view_city|
     - ``ICON_VIEW_CITY``
     -
     - |civ5_gak|
   * - |civ5_icon_worker|
     - ``ICON_WORKER``
     -
     - |civ5_gak|
   * - |civ5_icon_denounce|
     - ``ICON_DENOUNCE``
     -
     - |civ5_gak|
   * - |civ5_icon_invest|
     - ``ICON_INVEST``
     -
     - |civ5_gak|

   * - |civ5_icon_silver_fist|
     - ``ICON_SILVER_FIST``
     -
     - |civ5_bnw|
   * - |civ5_icon_checkbox|
     - ``ICON_CHECKBOX``
     -
     - |civ5_bnw|
   * - |civ5_icon_tourism|
     - ``ICON_TOURISM``
     -
     - |civ5_bnw|
   * - |civ5_icon_international_trade|
     - ``ICON_INTERNATIONAL_TRADE``
     -
     - |civ5_bnw|
   * - |civ5_icon_great_work|
     - ``ICON_GREAT_WORK``
     -
     - |civ5_bnw|
   * - |civ5_icon_arrow_left|
     - ``ICON_ARROW_LEFT``
     -
     - |civ5_bnw|
   * - |civ5_icon_arrow_right|
     - ``ICON_ARROW_RIGHT``
     - Shares the same icon as ``ICON_MOVES``
     - |civ5_bnw|
   * - |civ5_icon_victory_space|
     - ``ICON_VICTORY_SPACE``
     -
     - |civ5_bnw|
   * - |civ5_icon_victory_culture|
     - ``ICON_VICTORY_CULTURE``
     -
     - |civ5_bnw|
   * - |civ5_icon_victory_diplomacy|
     - ``ICON_VICTORY_DIPLOMACY``
     -
     - |civ5_bnw|
   * - |civ5_icon_victory_domination|
     - ``ICON_VICTORY_DOMINATION``
     -
     - |civ5_bnw|
   * - |civ5_icon_turns_remaining|
     - ``ICON_TURNS_REMAINING``
     - Shares the same icon as ``ICON_SWAP``
     - |civ5_bnw|
   * - |civ5_icon_great_engineer|
     - ``ICON_GREAT_ENGINEER``
     -
     - |civ5_bnw|
   * - |civ5_icon_great_general|
     - ``ICON_GREAT_GENERAL``
     -
     - |civ5_bnw|
   * - |civ5_icon_great_scientist|
     - ``ICON_GREAT_SCIENTIST``
     -
     - |civ5_bnw|
   * - |civ5_icon_great_merchant|
     - ``ICON_GREAT_MERCHANT``
     -
     - |civ5_bnw|
   * - |civ5_icon_great_artist|
     - ``ICON_GREAT_ARTIST``
     -
     - |civ5_bnw|
   * - |civ5_icon_great_musician|
     - ``ICON_GREAT_MUSICIAN``
     -
     - |civ5_bnw|
   * - |civ5_icon_great_writer|
     - ``ICON_GREAT_WRITER``
     -
     - |civ5_bnw|
   * - |civ5_icon_great_admiral|
     - ``ICON_GREAT_ADMIRAL``
     -
     - |civ5_bnw|
   * - |civ5_icon_great_merchant_venice|
     - ``ICON_GREAT_MERCHANT_VENICE``
     -
     - |civ5_bnw|
   * - |civ5_icon_great_explorer|
     - ``ICON_GREAT_EXPLORER``
     -
     - |civ5_bnw|
   * - |civ5_icon_res_cloves|
     - ``ICON_RES_CLOVES``
     -
     - |civ5_bnw|
   * - |civ5_icon_res_nutmeg|
     - ``ICON_RES_NUTMEG``
     -
     - |civ5_bnw|
   * - |civ5_icon_res_pepper|
     - ``ICON_RES_PEPPER``
     -
     - |civ5_bnw|
   * - |civ5_icon_diplomat|
     - ``ICON_DIPLOMAT``
     -
     - |civ5_bnw|
   * - |civ5_icon_res_artifacts|
     - ``ICON_RES_ARTIFACTS``
     -
     - |civ5_bnw|
   * - |civ5_icon_trophy_gold|
     - ``ICON_TROPHY_GOLD``
     -
     - |civ5_bnw|
   * - |civ5_icon_trophy_silver|
     - ``ICON_TROPHY_SILVER``
     -
     - |civ5_bnw|
   * - |civ5_icon_trophy_bronze|
     - ``ICON_TROPHY_BRONZE``
     -
     - |civ5_bnw|
   * - |civ5_icon_res_manpower|
     - ``ICON_RES_MANPOWER``
     -
     - |civ5_bnw|
   * - |civ5_icon_res_hidden_artifacts|
     - ``ICON_RES_HIDDEN_ARTIFACTS``
     -
     - |civ5_bnw|
   * - |civ5_icon_ideology_order|
     - ``ICON_IDEOLOGY_ORDER``
     -
     - |civ5_bnw|
   * - |civ5_icon_ideology_freedom|
     - ``ICON_IDEOLOGY_FREEDOM``
     -
     - |civ5_bnw|
   * - |civ5_icon_ideology_autocracy|
     - ``ICON_IDEOLOGY_AUTOCRACY``
     -
     - |civ5_bnw|
   * - |civ5_icon_res_cocoa|
     - ``ICON_RES_COCOA``
     -
     - |civ5_bnw|
   * - |civ5_icon_res_bison|
     - ``ICON_RES_BISON``
     -
     - |civ5_bnw|

Sources
^^^^^^^

- Icon definitions (Maps icon names to glyph IDs)
   - ``Assets/Gameplay/XML/GameInfo/CIV5IconFontMapping.xml``
   - ``Assets/DLC/Expansion/Gameplay/XML/GameInfo/CIV5IconFontMapping_Expansion.xml``
   - ``Assets/DLC/Expansion2/Gameplay/XML/GameInfo/CIV5IconFontMapping_Expansion2.xml``

- Glyph definitions (Maps glyph IDs to their area in the texture atlases)
   - ``Assets/UI/Fonts/Font Icons/FontIcons.ggxml``
   - ``Assets/DLC/Expansion2/UI/Fonts/Font Icons/FontIcons.ggxml``
   - ``Assets/DLC/Expansion2/UI/Fonts/Font Icons/FontIcons_Expansion2.ggxml``
   - ``Assets/DLC/Shared/UI/Art/Fonts/Font Icons/FontIcons.ggxml``
   - ``Assets/DLC/Shared/UI/Art/Fonts/Font Icons/FontIcons_DLC_07.ggxml``

- Texture atlases
   - ``Resource/DX9/UITextures.fpk`` > ``fonticons.dds``, ``fonticons-index.dds``
   - ``Resource/DX9/Expansion2UITextures.fpk`` > ``fonticons.dds``, ``fonticons-index.dds``
   - ``Resource/DX9/Expansion2UITextures.fpk`` > ``fonticons_expansion2.dds``, ``fonticons_expansion2-index.dds``
   - ``Assets/DLC/Shared/UI/Art/Fonts/Font Icons/FontIcons_DLC_07.dds``
