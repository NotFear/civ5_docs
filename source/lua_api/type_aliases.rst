Type aliases
============

.. contents:: :local:

PlayerTypes
-----------

.. lua:alias:: PlayerTypes = int

   A player ID

   .. code-block:: lua

      NO_PLAYER = -1

TeamTypes
---------

.. lua:alias:: TeamTypes = int

   A team ID

   .. code-block:: lua

      NO_TEAM = -1

YieldTypes
----------

.. lua:alias:: YieldTypes = int

   A yield type

   .. code-block:: lua

      NO_YIELD = -1

      YIELD_FOOD = 0
      YIELD_PRODUCTION = 1
      YIELD_GOLD = 2
      YIELD_SCIENCE = 3
      YIELD_CULTURE = 4
      YIELD_FAITH = 5

      NUM_YIELD_TYPES = 6

ResourceTypes
-------------

.. lua:alias:: ResourceTypes = int

   A resource type

   .. code-block:: lua

      NO_RESOURCE = -1

ImprovementTypes
----------------

.. lua:alias:: ImprovementTypes = int

   A tile improvement type

   .. code-block:: lua

      NO_IMPROVEMENT = -1

UnitTypes
---------

.. lua:alias:: UnitTypes = int

   A unit type

   Unit types can be found in the ``Units`` table in the game database.

   .. code-block:: lua

      NO_UNIT = -1

UnitAITypes
-----------

.. lua:alias:: UnitAITypes = int

   .. code-block:: lua

      NO_UNITAI = -1

      UNITAI_UNKNOWN = 0 -- we don't know what to do with these units
      UNITAI_SETTLE = 1 -- these are Settlers
      UNITAI_WORKER = 2 -- these are Builders
      UNITAI_ATTACK = 3 -- use these to attack other units
      UNITAI_CITY_BOMBARD = 4 -- use these to attack cities
      UNITAI_FAST_ATTACK = 5 -- use these to pillage enemy improvements and attack barbarians
      UNITAI_DEFENSE = 6 -- these are units that are mainly in the floating defense force
      UNITAI_COUNTER = 7 -- these are counter-units to specific other units - these will likely need more logic in building and using
      UNITAI_RANGED = 8 -- units with ranged attacks
      UNITAI_CITY_SPECIAL = 9 -- more AA???
      UNITAI_EXPLORE = 10 -- scouts, etc.
      UNITAI_ARTIST = 11 -- great person
      UNITAI_SCIENTIST = 12 -- great person
      UNITAI_GENERAL = 13 -- great person
      UNITAI_MERCHANT = 14 -- great person
      UNITAI_ENGINEER = 15 -- great person
      UNITAI_ICBM = 16 -- nuke
      UNITAI_WORKER_SEA = 17 -- work boats
      UNITAI_ATTACK_SEA = 18 -- naval melee units
      UNITAI_RESERVE_SEA = 19 -- naval units used defensively
      UNITAI_ESCORT_SEA = 20 -- naval units tasked to defend embarked units
      UNITAI_EXPLORE_SEA = 21 -- naval units used for scouting
      UNITAI_ASSAULT_SEA = 22 -- naval ranged units
      UNITAI_SETTLER_SEA = 23 -- UNUSED in Civ 5
      UNITAI_CARRIER_SEA = 24 -- aircraft carrier
      UNITAI_MISSILE_CARRIER_SEA = 25 -- missile carrier
      UNITAI_PIRATE_SEA = 26 -- avast, ye
      UNITAI_ATTACK_AIR = 27 -- bombers
      UNITAI_DEFENSE_AIR = 28 -- fighters
      UNITAI_CARRIER_AIR = 29 -- planes on boats
      UNITAI_MISSILE_AIR = 30 -- cruise missiles
      UNITAI_PARADROP = 31 -- paratrooper
      UNITAI_SPACESHIP_PART = 32 -- spaceship part that needs to be taken to capital
      UNITAI_TREASURE = 33 -- treasure to return to your capital
      UNITAI_PROPHET = 34 -- great person
      UNITAI_MISSIONARY = 35 -- missionary
      UNITAI_INQUISITOR = 36 -- inquisitor
      UNITAI_ADMIRAL = 37 -- admiral
      UNITAI_TRADE_UNIT = 38 -- international trade unit
      UNITAI_ARCHAEOLOGIST = 39 -- archaeologist
      UNITAI_WRITER = 40 -- great person
      UNITAI_MUSICIAN = 41 -- great person

BuildingTypes
-------------

.. lua:alias:: BuildingTypes = int

   A building type

   .. code-block:: lua

      NO_BUILDING = -1

ProjectTypes
------------

.. lua:alias:: ProjectTypes = int

   A project type, such as the Manhattan Project, Apollo Program, or the various spaceship parts

   Project types can be found in the ``Projects`` table in the game database.

   .. code-block:: lua

      NO_PROJECT = -1

TaskTypes
---------

.. lua:alias:: TaskTypes = int

   A task that a city can perform

   See :lua:meth:`City.DoTask`

   .. code-block:: lua

      TASK_RAZE = 0
      TASK_UNRAZE = 1
      TASK_DISBAND = 2
      TASK_GIFT = 3
      TASK_SET_AUTOMATED_CITIZENS = 4
      TASK_SET_AUTOMATED_PRODUCTION = 5
      TASK_SET_EMPHASIZE = 6
      TASK_NO_AUTO_ASSIGN_SPECIALISTS = 7
      TASK_ADD_SPECIALIST = 8
      TASK_REMOVE_SPECIALIST = 9
      TASK_CHANGE_WORKING_PLOT = 10
      TASK_REMOVE_SLACKER = 11
      TASK_CLEAR_WORKING_OVERRIDE = 12
      TASK_HURRY = 13
      TASK_CONSCRIPT = 14
      TASK_CLEAR_ORDERS = 15
      TASK_RALLY_PLOT = 16
      TASK_CLEAR_RALLY_PLOT = 17
      TASK_RANGED_ATTACK = 18
      TASK_CREATE_PUPPET = 19
      TASK_ANNEX_PUPPET = 20

OrderTypes
----------

.. lua:alias:: OrderTypes = int

   An order that a city can spend production on to complete

   .. code-block:: lua

      NO_ORDER = -1

      -- Produce a unit
      ORDER_TRAIN = 0

      -- Construct a building
      ORDER_CONSTRUCT = 1

      -- Construct a project
      ORDER_CREATE = 2

      -- Create a specialist
      ORDER_PREPARE = 3

      -- Spend research on an ongoing process (usually wealth or research)
      ORDER_MAINTAIN = 4


SpecialistTypes
---------------

.. lua:alias:: SpecialistTypes = int

   A specialist

   .. code-block:: lua

      NO_SPECIALIST = -1

ProcessTypes
------------

.. lua:alias:: ProcessTypes = int

   A process that the city can perform instead of producing anything (such as gold generation, or research)

   .. code-block:: lua

      NO_PROCESS = -1
